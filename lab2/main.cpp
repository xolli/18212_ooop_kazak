#include "MyExceptions.h"
#include "Parser.h"
#include "Scheme.h"

#include <cstring>
#include <iostream>

bool checkReadFileScheme (Scheme s){
  auto blocks = s.get_blocks();
  if (blocks.size() > 0) {

  }
};

int main(int argc, char *argv[]) {
  if (argc < 2) {
    std::cerr << "No workflow filename" << std::endl;
    return 1;
  }
  std::string input_filename;
  std::string output_filename;
  for (int i = 1; i < argc; ++i) {
    if (i + 1 < argc && !strcmp(argv[i], "-i")) {
      input_filename = argv[i + 1];
    } else if (i + 1 < argc && !strcmp(argv[i], "-o")) {
      output_filename = argv[i + 1];
    }
  }
  FileParser scheme_description;
  Scheme s;
  try {
    s = scheme_description.parse(std::string(argv[1]));
  } catch (ConfigFileException &exception) {
    std::cerr << exception.what() << std::endl;
    return 1;
  }

  try {
    SchemeExecutor::process(s, input_filename, output_filename);
  } catch (SchemeException &exception) {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  return 0;
}
