#include "WorkerFactory.h"

WorkerFactory::WorkerFactory() {
  factoryMap[WorkerType::READFILE] = new WorkerCreator<ReadFileWorker>;
  factoryMap[WorkerType::WRITEFILE] = new WorkerCreator<WriteFileWorker>;
  factoryMap[WorkerType::GREP] = new WorkerCreator<GrepWorker>;
  factoryMap[WorkerType::SORT] = new WorkerCreator<SortWorker>;
  factoryMap[WorkerType::REPLACE] = new WorkerCreator<ReplaceWorker>;
  factoryMap[WorkerType::DUMP] = new WorkerCreator<DumpWorker>;
}

WorkerFactory::~WorkerFactory() {
  for (auto elem : factoryMap) {
    delete elem.second;
    elem.second = nullptr;
  }
}

std::shared_ptr<Worker> WorkerFactory::create(const WorkerType type,
                                              const Arguments &args) const {
  auto it = factoryMap.find(type);
  if (it != factoryMap.end()) {
    return it->second->create(args);
  } else {
    return nullptr;
  }
}
