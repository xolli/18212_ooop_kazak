#include "Validator.h"
#include "MyExceptions.h"
#include <stdexcept>

WorkerMetaInfo::WorkerMetaInfo(const std::string &line) {
  auto equal = line.find(" = ");
  if (equal == std::string::npos) {
    throw ConfigFileException("No character \"=\" in description worker");
  }
  try {
    id = std::stoi(line.substr(0, equal));
  } catch (std::invalid_argument) {
    throw ConfigFileException("Invalid id");
  } catch (std::out_of_range) {
    throw ConfigFileException("Too big id");
  }
  std::string nameWorker;
  std::string other = line.substr(equal + 3);
  for (auto it = other.begin(); it != other.end() && *it != ' '; ++it) {
    nameWorker += *it;
  }
  if (nameWorker == "readfile") {
    type = WorkerType::READFILE;
  } else if (nameWorker == "writefile") {
    type = WorkerType::WRITEFILE;
  } else if (nameWorker == "grep") {
    type = WorkerType::GREP;
  } else if (nameWorker == "sort") {
    type = WorkerType::SORT;
  } else if (nameWorker == "replace") {
    type = WorkerType::REPLACE;
  } else if (nameWorker == "dump") {
    type = WorkerType::DUMP;
  } else {
    throw ConfigFileException("Invalid worker name");
  }
  args = Arguments(line.substr(equal + 3 + nameWorker.length()));
}

int WorkerMetaInfo::get_id() { return id; }

WorkerType WorkerMetaInfo::get_type() { return type; }

Arguments WorkerMetaInfo::get_args() { return args; }

bool ReadFileValidator::isSupport(WorkerMetaInfo workerInfo) {
  return workerInfo.get_type() == WorkerType::READFILE;
}

bool ReadFileValidator::validate(WorkerMetaInfo workerInfo) {
  return workerInfo.get_args().size() == 1;
}

bool WriteFileValidator::isSupport(WorkerMetaInfo workerInfo) {
  return workerInfo.get_type() == WorkerType::WRITEFILE;
}

bool WriteFileValidator::validate(WorkerMetaInfo workerInfo) {
  return workerInfo.get_args().size() == 1;
}

bool GrepValidator::isSupport(WorkerMetaInfo workerInfo) {
  return workerInfo.get_type() == WorkerType::GREP;
}

bool GrepValidator::validate(WorkerMetaInfo workerInfo) {
  return workerInfo.get_args().size() == 1;
}

bool SortValidator::isSupport(WorkerMetaInfo workerInfo) {
  return workerInfo.get_type() == WorkerType::SORT;
}

bool SortValidator::validate(WorkerMetaInfo workerInfo) {
  return workerInfo.get_args().size() == 0;
}

bool ReplaceValidator::isSupport(WorkerMetaInfo workerInfo) {
  return workerInfo.get_type() == WorkerType::REPLACE;
}

bool ReplaceValidator::validate(WorkerMetaInfo workerInfo) {
  return workerInfo.get_args().size() == 2;
}

bool DumpValidator::isSupport(WorkerMetaInfo workerInfo) {
  return workerInfo.get_type() == WorkerType::DUMP;
}

bool DumpValidator::validate(WorkerMetaInfo workerInfo) {
  return workerInfo.get_args().size() == 1;
}
