#ifndef TEST_VALIDATOR_H
#define TEST_VALIDATOR_H

#include "Worker.h"

class WorkerMetaInfo {
  int id;
  WorkerType type;
  Arguments args;

public:
  explicit WorkerMetaInfo(const std::string &);

  int get_id();

  WorkerType get_type();

  Arguments get_args();
};

class Validator {
public:
  virtual bool isSupport(WorkerMetaInfo) = 0;

  virtual bool validate(WorkerMetaInfo) = 0;
};

class ReadFileValidator : public Validator {
  bool isSupport(WorkerMetaInfo) override;

  bool validate(WorkerMetaInfo) override;
};

class WriteFileValidator : public Validator {
  bool isSupport(WorkerMetaInfo) override;

  bool validate(WorkerMetaInfo) override;
};

class GrepValidator : public Validator {
  bool isSupport(WorkerMetaInfo) override;

  bool validate(WorkerMetaInfo) override;
};

class SortValidator : public Validator {
  bool isSupport(WorkerMetaInfo) override;

  bool validate(WorkerMetaInfo) override;
};

class ReplaceValidator : public Validator {
  bool isSupport(WorkerMetaInfo) override;

  bool validate(WorkerMetaInfo) override;
};

class DumpValidator : public Validator {
  bool isSupport(WorkerMetaInfo) override;

  bool validate(WorkerMetaInfo) override;
};

#endif // TEST_VALIDATOR_H
