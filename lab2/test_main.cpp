#include <fstream>
#include <gtest/gtest.h>

#include "MyExceptions.h"
#include "Parser.h"
#include "Validator.h"
#include "Worker.h"

std::string read_all_file(const std::string &filename) {
  std::ifstream fin(filename);
  fin.seekg(0, fin.end);
  int size = fin.tellg();
  fin.seekg(0, fin.beg);

  std::string s(size, '\0');

  fin.read(&s[0], size); // read all text
  fin.close();
  return s;
}

TEST(Arguments, simple_test) {
  Arguments test("1 2 3");
  for (int i = 1; i <= 3; ++i) {
    std::string h = test.get_argument();
    EXPECT_EQ(std::to_string(i), h);
  }
  EXPECT_EQ("", test.get_argument());
}

TEST(Arguments, space_end) {
  Arguments test("1 2 3      ");
  for (int i = 1; i <= 3; ++i) {
    std::string h = test.get_argument();
    EXPECT_EQ(std::to_string(i), h);
  }
  EXPECT_EQ("", test.get_argument());
}

TEST(Arguments, space_begin) {
  Arguments test("       1 2 3");
  for (int i = 1; i <= 3; ++i) {
    std::string h = test.get_argument();
    EXPECT_EQ(std::to_string(i), h);
  }
  EXPECT_EQ("", test.get_argument());
}

TEST(Arguments, default_value) {
  Arguments test;
  EXPECT_EQ("", test.get_argument());
}

TEST(Arguments, many_spaces) {
  Arguments test("     word1    word2    word3     word4      ");
  for (int i = 1; i <= 4; ++i) {
    std::string h = test.get_argument();
    EXPECT_EQ("word" + std::to_string(i), h);
  }
  EXPECT_EQ("", test.get_argument());
}

TEST(FileReaderWorker, read) {
  std::ofstream fout("test_file.txt");
  fout << "abcd\n"
          "abba\n"
          "1234567890";
  fout.close();

  Arguments args("test_file.txt");
  ReadFileWorker block(args);
  std::string read_text = block.process("");
  EXPECT_EQ("abcd\n"
            "abba\n"
            "1234567890",
            read_text);
  std::remove("test_file.txt");
}

TEST(FileWriterWorker, write) {
  std::string text = "1234567890-=\n"
                     "random_text____\n\n\t\n\n";

  WriteFileWorker block(Arguments("test_file.txt"));
  block.process(text);

  std::string s = read_all_file("test_file.txt");
  std::remove("test_file.txt");

  EXPECT_EQ("1234567890-=\n"
            "random_text____\n\n\t\n\n",
            s);
}

TEST(GrepWorker, simple_test) {
  GrepWorker block(Arguments("123"));
  std::string text = "123\n"
                     "456\n"
                     "123\n"
                     "789\n"
                     "text 123 text\n"
                     "123 text\n"
                     "text 123\n";
  std::string result = block.process(text);
  EXPECT_EQ("123\n"
            "123\n"
            "text 123 text\n"
            "123 text\n"
            "text 123\n",
            result);
}

TEST(GrepWorker, last_line) {
  GrepWorker block(Arguments("FIND"));
  std::string text = "FIND\n"
                     "456\n"
                     "FINDFINDFINDFIND\n"
                     "789\n"
                     "text 123 text\n"
                     "123 text\n"
                     "FIND";
  std::string result = block.process(text);
  EXPECT_EQ("FIND\n"
            "FINDFINDFINDFIND\n"
            "FIND",
            result);
}

TEST(GrepWorker, empty_word) {
  GrepWorker block(Arguments(""));
  std::string text = "FIND\n"
                     "456\n"
                     "FINDFINDFINDFIND\n"
                     "789\n"
                     "text 123 text\n"
                     "123 text\n"
                     "FIND";
  std::string result = block.process(text);
  EXPECT_EQ("FIND\n"
            "456\n"
            "FINDFINDFINDFIND\n"
            "789\n"
            "text 123 text\n"
            "123 text\n"
            "FIND",
            result);
}

TEST(GrepWorker, empty_line) {
  GrepWorker block(Arguments("FIND"));
  std::string text = "\n\n\n\n\n\n\n\n\nFIND\n\n\n\n\n\n\n\n\n\n\n\n";
  std::string result = block.process(text);
  EXPECT_EQ("FIND\n", result);
}

TEST(GrepWorker, empty_line_empty_word) {
  GrepWorker block(Arguments(""));
  std::string text = "\n\n\n\n\n\n\n\n\nFIND\n\n\n\n\n\n\n\n\n\n\n\n";
  std::string result = block.process(text);
  EXPECT_EQ("\n\n\n\n\n\n\n\n\nFIND\n\n\n\n\n\n\n\n\n\n\n\n", result);
}

TEST(GrepWorker, empty_result) {
  GrepWorker block(Arguments("FIND"));
  std::string text = "qwerty\n"
                     "random word\n"
                     "hello world! =)";
  std::string result = block.process(text);
  EXPECT_EQ("", result);
}

TEST(GrepWorker, register_symbol) {
  GrepWorker block(Arguments("fInD"));
  std::string text = "find\n"
                     "Find\n"
                     "fInd\n"
                     "fiNd\n"
                     "finD\n"
                     "FInd\n"
                     "FiNd\n"
                     "FinD\n"
                     "fINd\n"
                     "fInD\n"
                     "fiND\n"
                     "FINd\n"
                     "FInD\n"
                     "FiND\n"
                     "fIND\n"
                     "FIND\n";
  std::string result = block.process(text);
  EXPECT_EQ("fInD\n", result);
}

TEST(SortWorker, simple_test) {
  SortWorker block;
  std::string result = block.process("qwertyui\n"
                                     "1234567890\n"
                                     "0123456789\n"
                                     "abcdefg\n"
                                     "abc");
  EXPECT_EQ("0123456789\n"
            "1234567890\n"
            "abc\n"
            "abcdefg\n"
            "qwertyui",
            result);
}

TEST(SortWorker, empty_line_in_end) {
  SortWorker block;
  std::string result = block.process("qwertyui\n"
                                     "1234567890\n"
                                     "0123456789\n"
                                     "abcdefg\n"
                                     "abc\n");
  EXPECT_EQ("0123456789\n"
            "1234567890\n"
            "abc\n"
            "abcdefg\n"
            "qwertyui",
            result);
}

TEST(SortWorker, empty_line) {
  SortWorker block;
  std::string result = block.process("qwertyui\n"
                                     "1234567890\n"
                                     "\n"
                                     "\n"
                                     "0123456789\n"
                                     "\n"
                                     "abcdefg\n"
                                     "abc");
  EXPECT_EQ("\n"
            "\n"
            "\n"
            "0123456789\n"
            "1234567890\n"
            "abc\n"
            "abcdefg\n"
            "qwertyui",
            result);
}

TEST(SortWorker, only_empty_line) {
  SortWorker block;
  std::string result = block.process("\n\n\n\n\n"); // "\n" * 5
  EXPECT_EQ("\n\n\n\n", result);                    // "\n" * 4
}

TEST(ReplaceWorker, simple_test) {
  ReplaceWorker block(Arguments("word1 WORD2"));
  std::string result = block.process("This is a string. word1.");
  EXPECT_EQ("This is a string. WORD2.", result);
}

TEST(ReplaceWorker, simple_test_2) {
  ReplaceWorker block(Arguments("word1 WORD2"));
  std::string result = block.process("This is word1 a string. word1.");
  EXPECT_EQ("This is WORD2 a string. WORD2.", result);
}

TEST(ReplaceWorker, simple_test_3) {
  ReplaceWorker block(Arguments("word1 WORD2"));
  std::string result = block.process("word1word1word1");
  EXPECT_EQ("WORD2WORD2WORD2", result);
}

TEST(ReplaceWorker, doubling) {
  ReplaceWorker block(Arguments("a aa"));
  std::string result = block.process("aaa");
  EXPECT_EQ("aaaaaa", result);
}

TEST(ReplaceWorker, abba) {
  ReplaceWorker block(Arguments("abba x"));
  std::string result = block.process("abbabba");
  EXPECT_EQ("xbba", result);
}

TEST(ReplaceWorker, abba2) {
  ReplaceWorker block(Arguments("abba x"));
  std::string result = block.process("abbabbabbabbabbabbabba");
  EXPECT_EQ("xbbxbbxbbx", result);
}

TEST(ReplaceWorker, delete_word) {
  ReplaceWorker block(Arguments("abba"));
  std::string result = block.process("abba is a great musical band!");
  EXPECT_EQ(" is a great musical band!", result);
}

TEST(ReplaceWorker, empty_output) {
  ReplaceWorker block(Arguments("abba"));
  std::string result = block.process("abba");
  EXPECT_EQ("", result);
}

TEST(ReplaceWorker, no_aruments) {
  ReplaceWorker block(Arguments(""));
  std::string result = block.process("This is a string");
  EXPECT_EQ("This is a string", result);
}

TEST(DumpWorker, simple_test) {
  DumpWorker block(Arguments("test_file.txt"));
  std::string text = "This is a test string\n";
  block.process(text);

  std::string s = read_all_file("test_file.txt");
  std::remove("test_file.txt");

  EXPECT_EQ("This is a test string\n", s);
}

TEST(WorkerMetaInfo, ReadFile) {
  WorkerMetaInfo meta("0 = readfile in.txt");
  EXPECT_EQ(WorkerType::READFILE, meta.get_type());
  EXPECT_EQ(0, meta.get_id());
  ASSERT_EQ(1, meta.get_args().size());
  EXPECT_EQ("in.txt", meta.get_args().get_argument());
}

TEST(WorkerMetaInfo, WriteFile) {
  WorkerMetaInfo meta("5 = writefile out.txt");
  EXPECT_EQ(WorkerType::WRITEFILE, meta.get_type());
  EXPECT_EQ(5, meta.get_id());
  ASSERT_EQ(1, meta.get_args().size());
  EXPECT_EQ("out.txt", meta.get_args().get_argument());
}

TEST(WorkerMetaInfo, Grep) {
  WorkerMetaInfo meta("2 = grep braab");
  EXPECT_EQ(WorkerType::GREP, meta.get_type());
  EXPECT_EQ(2, meta.get_id());
  ASSERT_EQ(1, meta.get_args().size());
  EXPECT_EQ("braab", meta.get_args().get_argument());
}

TEST(WorkerMetaInfo, Sort) {
  WorkerMetaInfo meta("3 = sort");
  EXPECT_EQ(WorkerType::SORT, meta.get_type());
  EXPECT_EQ(3, meta.get_id());
  ASSERT_EQ(0, meta.get_args().size());
  EXPECT_EQ("", meta.get_args().get_argument());
}

TEST(WorkerMetaInfo, Replace) {
  WorkerMetaInfo meta("1 = replace abracadabra cadabraabra");
  EXPECT_EQ(WorkerType::REPLACE, meta.get_type());
  EXPECT_EQ(1, meta.get_id());
  Arguments args = meta.get_args();
  ASSERT_EQ(2, args.size());
  EXPECT_EQ("abracadabra", args.get_argument());
  EXPECT_EQ("cadabraabra", args.get_argument());
  EXPECT_EQ("", args.get_argument());
}

TEST(WorkerMetaInfo, Dump) {
  WorkerMetaInfo meta("100500 = dump check.txt");
  EXPECT_EQ(WorkerType::DUMP, meta.get_type());
  EXPECT_EQ(100500, meta.get_id());
  ASSERT_EQ(1, meta.get_args().size());
  EXPECT_EQ("check.txt", meta.get_args().get_argument());
}

TEST(Parser, correct_test1) {
  FileParser f;
  Scheme s = f.parse("../tests/test1.txt");
  SchemeExecutor().process(s);
  ASSERT_EQ("braabbraab\n"
            "cadabraabra\n"
            "cadabraabra",
            read_all_file("../tests/out1.txt"));
}

TEST(Parser, correct_test2) {
  FileParser f;
  Scheme s = f.parse("../tests/test2.txt");
  SchemeExecutor().process(s);
  ASSERT_EQ("word\n"
            "wordDDDDDD\n"
            "words",
            read_all_file("../tests/out2.txt"));
}

TEST(Parser, correct_test3) {
  FileParser f;
  Scheme s = f.parse("../tests/test3.txt");
  SchemeExecutor().process(s);
  EXPECT_EQ("AAAAAAAA\n"
            "BBBBBBBB\n",
            read_all_file("../tests/out3.txt"));
  EXPECT_EQ("AAAAAAAA\n"
            "bbbbbbbb\n",
            read_all_file("../tests/dump3.txt"));
}

TEST(Parser, correct_test4) {
  FileParser f;
  Scheme s = f.parse("../tests/test4.txt");
  SchemeExecutor().process(s);
  EXPECT_EQ("ABCDEFGHIJKLMNOPQRSYUVWXYZ\n"
            "ZYXWVUYSRQPONMLKJIHGFEDCBA\n"
            "ABC\n"
            "ABC\n"
            "ABBBBBBBBACD\n"
            "AABC\n"
            "ABCDEFG\n",
            read_all_file("../tests/dump4.txt"));
  EXPECT_EQ("ABCDEFGHIJKLMNOPQRSYUVWXYZ\n"
            "ABC\n"
            "ABC\n"
            "AABC\n"
            "ABCDEFG\n",
            read_all_file("../tests/out4.txt"));
}

TEST(Parser, correct_test5) {
  FileParser f;
  Scheme s = f.parse("../tests/test5.txt");
  SchemeExecutor().process(s);
  EXPECT_EQ(
      "matthew 2:23\tand he came and dwelt in a city called nazareth: that it "
      "might be fulfilled which was spoken by the prophets, he shall be called "
      "a nazarene.\r\n"
      "matthew 4:13\tand leaving nazareth, he came and dwelt in capernaum, "
      "which is upon the sea coast, in the borders of zabulon and "
      "nephthalim:\r\n"
      "matthew 21:11\tand the multitude said, this is jesus the prophet of "
      "nazareth of galilee.\r\n"
      "matthew 26:71\tand when he was gone out into the porch, another maid "
      "saw him, and said unto them that were there, this fellow was also with "
      "jesus of nazareth.\r\n"
      "mark 1:9\tand it came to pass in those days, that jesus came from "
      "nazareth of galilee, and was baptized of john in jordan.\r\n"
      "mark 1:24\tsaying, let us alone; what have we to do with thee, thou "
      "jesus of nazareth? art thou come to destroy us? i know thee who thou "
      "art, the holy one of god.\r\n"
      "mark 10:47\tand when he heard that it was jesus of nazareth, he began "
      "to cry out, and say, jesus, thou son of david, have mercy on me.\r\n"
      "mark 14:67\tand when she saw peter warming himself, she looked upon "
      "him, and said, and thou also wast with jesus of nazareth.\r\n"
      "mark 16:6\tand he saith unto them, be not affrighted: ye seek jesus of "
      "nazareth, which was crucified: he is risen; he is not here: behold the "
      "place where they laid him.\r\n"
      "luke 1:26\tand in the sixth month the angel gabriel was sent from god "
      "unto a city of galilee, named nazareth,\r\n"
      "luke 2:4\tand joseph also went up from galilee, out of the city of "
      "nazareth, into judaea, unto the city of david, which is called "
      "bethlehem; (because he was of the house and lineage of david:)\r\n"
      "luke 2:39\tand when they had performed all things according to the law "
      "of the lord, they returned into galilee, to their own city nazareth.\r\n"
      "luke 2:51\tand he went down with them, and came to nazareth, and was "
      "subject unto them: but his mother kept all these sayings in her "
      "heart.\r\n"
      "luke 4:16\tand he came to nazareth, where he had been brought up: and, "
      "as his custom was, he went into the synagogue on the sabbath day, and "
      "stood up for to read.\r\n"
      "luke 4:34\tsaying, let us alone; what have we to do with thee, thou "
      "jesus of nazareth? art thou come to destroy us? i know thee who thou "
      "art; the holy one of god.\r\n"
      "luke 18:37\tand they told him, that jesus of nazareth passeth by.\r\n"
      "luke 24:19\tand he said unto them, what things? and they said unto him, "
      "concerning jesus of nazareth, which was a prophet mighty in deed and "
      "word before god and all the people:\r\n"
      "john 1:45\tphilip findeth nathanael, and saith unto him, we have found "
      "him, of whom moses in the law, and the prophets, did write, jesus of "
      "nazareth, the son of joseph.\r\n"
      "john 1:46\tand nathanael said unto him, can there any good thing come "
      "out of nazareth? philip saith unto him, come and see.\r\n"
      "john 18:5\tthey answered him, jesus of nazareth. jesus saith unto them, "
      "i am he. and judas also, which betrayed him, stood with them.\r\n"
      "john 18:7\tthen asked he them again, whom seek ye? and they said, jesus "
      "of nazareth.\r\n"
      "john 19:19\tand pilate wrote a title, and put it on the cross. and the "
      "writing was jesus of nazareth the king of the jews.\r\n"
      "acts 2:22\tye men of israel, hear these words; jesus of nazareth, a man "
      "approved of god among you by miracles and wonders and signs, which god "
      "did by him in the midst of you, as ye yourselves also know:\r\n"
      "acts 3:6\tthen peter said, silver and gold have i none; but such as i "
      "have give i thee: in the name of jesus christ of nazareth rise up and "
      "walk.\r\n"
      "acts 4:10\tbe it known unto you all, and to all the people of israel, "
      "that by the name of jesus christ of nazareth, whom ye crucified, whom "
      "god raised from the dead, even by him doth this man stand here before "
      "you whole.\r\n"
      "acts 6:14\tfor we have heard him say, that this jesus of nazareth shall "
      "destroy this place, and shall change the customs which moses delivered "
      "us.\r\n"
      "acts 10:38\thow god anointed jesus of nazareth with the holy ghost and "
      "with power: who went about doing good, and healing all that were "
      "oppressed of the devil; for god was with him.\r\n"
      "acts 22:8\tand i answered, who art thou, lord? and he said unto me, i "
      "am jesus of nazareth, whom thou persecutest.\r\n"
      "acts 26:9\ti verily thought with myself, that i ought to do many things "
      "contrary to the name of jesus of nazareth.\r\n",
      read_all_file("../tests/out5.txt"));
}

TEST(Parser, correct_test6) {
  FileParser f;
  Scheme s = f.parse("../tests/test6.txt");
  SchemeExecutor().process(s, "../tests/in6.txt", "../tests/out6.txt");
  EXPECT_EQ("some_word1\n"
            "some_word2\n"
            "some_word3\n"
            "some_word4",
            read_all_file("../tests/out6.txt"));
}

TEST(Exception, error_test1) {
  FileParser f;
  Scheme s;
  EXPECT_ANY_THROW(s = f.parse("../tests_error/test1.txt"));
}

TEST(Exception, error_test2) {
  FileParser f;
  Scheme s;
  EXPECT_ANY_THROW(s = f.parse("../tests_error/test2.txt"));
}

TEST(Exception, error_test3) {
  FileParser f;
  Scheme s;
  EXPECT_ANY_THROW(s = f.parse("../tests_error/test3.txt"));
}

TEST(Exception, error_test4) {
  FileParser f;
  Scheme s;
  s = f.parse("../tests_error/test4.txt");
  EXPECT_ANY_THROW(SchemeExecutor().process(s));
}

TEST(Exception, error_test5) {
  FileParser f;
  Scheme s;
  EXPECT_ANY_THROW(s = f.parse("../tests_error/test5.txt"));
}

TEST(Exception, error_test6) {
  FileParser f;
  Scheme s;
  s = f.parse("../tests_error/test6.txt");
  EXPECT_ANY_THROW(SchemeExecutor().process(s));
}

TEST(Exception, error_test7) {
  FileParser f;
  Scheme s;
  s = f.parse("../tests_error/test7.txt");
  EXPECT_ANY_THROW(SchemeExecutor().process(s));
}

TEST(Exception, error_test8) {
  FileParser f;
  Scheme s;
  EXPECT_ANY_THROW(s = f.parse("../tests_error/test8.txt"));
}

TEST(Exception, error_test9) {
  FileParser f;
  Scheme s;
  s = f.parse("../tests_error/test9.txt");
  EXPECT_ANY_THROW(SchemeExecutor().process(s));
}

TEST(Exception, error_test10) {
  FileParser f;
  Scheme s;
  EXPECT_ANY_THROW(s = f.parse("../tests_error/test10.txt"));
}

TEST(Exception, error_test11) {
  FileParser f;
  Scheme s = f.parse("../tests_error/test11.txt");
  EXPECT_ANY_THROW(SchemeExecutor().process(s));
}

TEST(Exception, error_test12) {
  FileParser f;
  EXPECT_ANY_THROW(f.parse("../tests_error/test12.txt"));
}

TEST(Exception, error_test13) {
  FileParser f;
  Scheme s = f.parse("../tests_error/test13.txt");
  EXPECT_ANY_THROW(SchemeExecutor().process(s));
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
