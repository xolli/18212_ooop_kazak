#ifndef LAB2_V2_MYEXCEPTIONS_H
#define LAB2_V2_MYEXCEPTIONS_H

#include <exception>
#include <string>

class ConfigFileException : public std::exception {
  std::string description_error;

public:
  explicit ConfigFileException(std::string error) : description_error(error){};

  const char *what() const noexcept override {
    return description_error.c_str();
  };
};

class WorkerException : public std::exception {
  std::string description_error;

public:
  explicit WorkerException(std::string error) : description_error(error){};

  const char *what() const noexcept override {
    return description_error.c_str();
  };
};

class SchemeException : public std::exception {
  std::string description_error;

public:
  explicit SchemeException(std::string error) : description_error(error){};

  const char *what() const noexcept override {
    return description_error.c_str();
  };
};

#endif // LAB2_V2_MYEXCEPTIONS_H
