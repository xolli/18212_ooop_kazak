#include "Scheme.h"
#include "MyExceptions.h"
#include "WorkerFactory.h"

#include <string>

Scheme::Scheme(std::map<int, std::shared_ptr<Worker>> &workers_id,
               std::vector<int> &vector_id)
    : blocks(workers_id), struct_scheme(vector_id) {}

std::map<int, std::shared_ptr<Worker>> Scheme::get_blocks() { return blocks; }

std::vector<int> Scheme::get_struct_scheme() { return struct_scheme; }

std::shared_ptr<Worker>
SchemeExecutor::get_readfile_worker(const std::string &filename) {
  WorkerFactory factory;
  return factory.create(WorkerType::READFILE, Arguments(filename));
}

std::shared_ptr<Worker>
SchemeExecutor::get_writefile_worker(const std::string &filename) {
  WorkerFactory factory;
  return factory.create(WorkerType::WRITEFILE, Arguments(filename));
}

void SchemeExecutor::process(Scheme s, std::string input_filename,
                            std::string output_filename) {
  std::string buffer;
  auto blocks = s.get_blocks();
  auto struct_scheme = s.get_struct_scheme();
  auto size = struct_scheme.size();
  if (size > 0 && blocks.find(struct_scheme[0]) == blocks.end()) {
    throw SchemeException("ID does not exist");
  }
  if (size > 0 && blocks[struct_scheme[0]]->type() != WorkerType::READFILE &&
      input_filename.empty()) {
    throw SchemeException("Incorrect first block");
  } else if (size > 0 &&
             blocks[struct_scheme[0]]->type() != WorkerType::READFILE &&
             !input_filename.empty()) {
    auto readFile = get_readfile_worker(input_filename);
    buffer = readFile->process(buffer);
  }
  std::shared_ptr<Worker> writeFile;
  if (size > 0 && blocks.find(struct_scheme[size - 1]) == blocks.end()) {
    throw SchemeException("ID does not exist");
  }
  if (size > 0 &&
      blocks[struct_scheme[size - 1]]->type() != WorkerType::WRITEFILE &&
      output_filename.empty()) {
    throw SchemeException("Incorrect last block");
  } else if (size > 0 &&
             blocks[struct_scheme[size - 1]]->type() != WorkerType::WRITEFILE &&
             !output_filename.empty()) {
    writeFile = get_writefile_worker(output_filename);
  }
  int prev_id = -1;
  for (size_t i = 0; i < struct_scheme.size(); ++i) {
    int id = struct_scheme[i];
    if (id < 0) {
      throw SchemeException("Invalid ID");
    }
    if (i > 1 && (blocks[prev_id]->type() == WorkerType::READFILE ||
                  blocks[prev_id]->type() == WorkerType::WRITEFILE)) {
      throw SchemeException(
          "Readfile or writefile in the middle of the conveyor");
    }
    if (blocks.find(id) == blocks.end()) {
      throw SchemeException("ID does not exist");
    }
    try {
      buffer = blocks[id]->process(buffer);
    } catch (WorkerException &exception) {
      throw SchemeException(exception.what());
    }
    prev_id = id;
  }
  if (size > 0 &&
      blocks[struct_scheme[size - 1]]->type() != WorkerType::WRITEFILE &&
      !output_filename.empty()) {
    writeFile->process(buffer);
  }
}