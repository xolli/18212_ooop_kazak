#ifndef TEST_WORKERFACTORY_H
#define TEST_WORKERFACTORY_H

#include "Validator.h" // WorkerType
#include "Worker.h"
#include <map>
#include <memory>

class AbstractWorkerCreator {
public:
  virtual std::shared_ptr<Worker> create(const Arguments &) = 0;
};

template <class W> class WorkerCreator : public AbstractWorkerCreator {
public:
  std::shared_ptr<Worker> create(const Arguments &args) override {
    return std::make_shared<W>(args);
  };

  ~WorkerCreator() = default;
};

class WorkerFactory {
  std::map<WorkerType, AbstractWorkerCreator *> factoryMap;

public:
  WorkerFactory();

  ~WorkerFactory();

  std::shared_ptr<Worker> create(WorkerType, const Arguments &) const;
};

#endif // TEST_WORKERFACTORY_H
