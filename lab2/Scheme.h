#ifndef TEST_EXECUTOR2_H
#define TEST_EXECUTOR2_H

#include "MyExceptions.h"
#include "Worker.h"

#include <map>
#include <memory> // shared_ptr
#include <vector>

class Scheme {
  std::map<int, std::shared_ptr<Worker>> blocks;
  std::vector<int> struct_scheme;

public:
  Scheme() = default;
  Scheme(std::map<int, std::shared_ptr<Worker>> &, std::vector<int> &);
  std::map<int, std::shared_ptr<Worker>> get_blocks();
  std::vector<int> get_struct_scheme();
};

class SchemeExecutor {
  static std::shared_ptr<Worker>
  get_readfile_worker(const std::string &filename);
  static std::shared_ptr<Worker>
  get_writefile_worker(const std::string &filename);

public:
  static void process(Scheme s, std::string input_filename = "",
                      std::string output_filename = "");
};

#endif // TEST_EXECUTOR2_H
