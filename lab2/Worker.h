#ifndef LAB2_V2_WORKER_H
#define LAB2_V2_WORKER_H

#include <string>
#include <vector>

enum class WorkerType { READFILE, WRITEFILE, GREP, SORT, REPLACE, DUMP };

class Arguments {
  uint index_arg;
  std::vector<std::string> args;

public:
  Arguments() = default;
  explicit Arguments(const std::string &);
  Arguments(const Arguments &);
  std::string get_argument();
  int size();
};

class Worker {
protected:
  Arguments args;

public:
  explicit Worker(const Arguments &);
  Worker() = default;
  virtual std::string process(std::string) = 0;
  virtual WorkerType type() = 0;
};

class ReadFileWorker : public Worker {
public:
  using Worker::Worker;
  std::string process(std::string) override;
  WorkerType type() override;
};

class WriteFileWorker : public Worker {
public:
  using Worker::Worker;
  std::string process(std::string) override;
  WorkerType type() override;
};

class GrepWorker : public Worker {
public:
  using Worker::Worker;
  std::string process(std::string) override;
  WorkerType type() override;
};

class SortWorker : public Worker {
public:
  using Worker::Worker;
  std::string process(std::string) override;
  WorkerType type() override;
};

class ReplaceWorker : public Worker {
public:
  using Worker::Worker;
  std::string process(std::string) override;
  WorkerType type() override;
};

class DumpWorker : public Worker {
public:
  using Worker::Worker;
  std::string process(std::string) override;
  WorkerType type() override;
};

#endif // LAB2_V2_WORKER_H
