#ifndef TEST_PARSER_H
#define TEST_PARSER_H

#include "Scheme.h"
#include "Validator.h"
#include "WorkerFactory.h"

#include <string>
#include <vector>

class Parser {
protected:
  std::vector<Validator *> validators;
  WorkerFactory factory;

public:
  Parser();

  ~Parser();

  virtual Scheme parse(std::string const &filename) = 0;
};

class FileParser : Parser {
  std::map<int, std::shared_ptr<Worker>> get_blocks(std::ifstream &,
                                                    const std::string &);
  static std::vector<int> get_scheme_description(std::ifstream &,
                                                 const std::string &);

public:
  using Parser::Parser;

  Scheme parse(std::string const &filename) override;
};

#endif // TEST_PARSER_H
