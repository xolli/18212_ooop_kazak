#include "Parser.h"
#include "MyExceptions.h"

#include <cstring> // std::strcmp
#include <fstream>
#include <sstream> // std::istringstream

#define N_VALIDATORS 6

Parser::Parser() {
  validators.resize(N_VALIDATORS);
  validators[0] = new ReadFileValidator;
  validators[1] = new WriteFileValidator;
  validators[2] = new GrepValidator;
  validators[3] = new SortValidator;
  validators[4] = new ReplaceValidator;
  validators[5] = new DumpValidator;
}

Parser::~Parser() {
  for (auto validator : validators) {
    delete validator;
    validator = nullptr;
  }
}

std::map<int, std::shared_ptr<Worker>>
FileParser::get_blocks(std::ifstream &fin, const std::string &filename) {
  std::map<int, std::shared_ptr<Worker>> result;
  std::string line;
  try {
    std::getline(fin, line);
  } catch (std::ifstream::failure) {
    throw ConfigFileException("cannot read file \"" + filename + "\"");
  }
  while (!fin.eof() && line != "csed") {
    WorkerMetaInfo meta(line);
    for (auto validator : validators) {
      if (validator->isSupport(meta) && validator->validate(meta)) {
        if (result.find(meta.get_id()) != result.end()) {
          throw ConfigFileException("Replace id");
        }
        result[meta.get_id()] =
            factory.create(meta.get_type(), meta.get_args());
      } else if (validator->isSupport(meta) && !validator->validate(meta)) {
        throw ConfigFileException("invalid description of worker");
      }
    }
    try {
      std::getline(fin, line);
    } catch (std::ifstream::failure) {
      throw ConfigFileException("cannot read file \"" + filename + "\"");
    }
  }
  if (line != "csed") {
    throw ConfigFileException("No key word \"csed\"");
  }
  return result;
}

std::vector<int>
FileParser::get_scheme_description(std::ifstream &fin,
                                   const std::string &filename) {
  std::string line;
  std::vector<int> result;
  try {
    std::getline(fin, line);
  } catch (std::ifstream::failure) {
    throw ConfigFileException("cannot read file \"" + filename + "\"");
  }
  std::istringstream scheme_description(line);
  char separator[2];

  int id = 0;
  if (!(scheme_description >> id)) {
    //throw ConfigFileException("No conveyor");
    return result;
  }
  result.push_back(id);
  while (!scheme_description.eof()) {
    for (int i = 0; i < 2; ++i) {
      scheme_description >> separator[i];
    }
    if (std::strcmp(separator, "->")) {
      throw ConfigFileException("Invalid separator");
    }

    if (!(scheme_description >> id)) {
      throw ConfigFileException("Invalid ID in conveyor");
    }
    result.push_back(id);
  }
  return result;
}

Scheme FileParser::parse(std::string const &filename) {
  std::ifstream fin;
  fin.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try {
    fin.open(filename);
  } catch (std::ifstream::failure) {
    throw ConfigFileException("cannot open file \"" + filename + "\"");
  }
  std::string line;
  try {
    std::getline(fin, line);
  } catch (std::ifstream::failure) {
    throw ConfigFileException("cannot read file \"" + filename + "\"");
  }
  if (line != "desc") {
    throw ConfigFileException("No key word \"desc\"");
  }

  std::map<int, std::shared_ptr<Worker>> blocks = get_blocks(fin, filename);
  std::vector<int> a = get_scheme_description(fin, filename);
  fin.close();
  return Scheme(blocks, a);
}
