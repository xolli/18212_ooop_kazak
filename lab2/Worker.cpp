#include "Worker.h"
#include "MyExceptions.h"

#include <algorithm>
#include <fstream>
#include <string>

Arguments::Arguments(const std::string &input) {
  index_arg = 0;
  std::string new_arg;
  for (auto symbol : input) {
    if (symbol != ' ') {
      new_arg += symbol;
    } else if (!new_arg.empty()) {
      args.push_back(new_arg);
      new_arg = "";
    }
  }
  if (!new_arg.empty()) {
    args.push_back(new_arg);
  }
}

std::string Arguments::get_argument() {
  if (index_arg < args.size()) {
    ++index_arg;
    return args[index_arg - 1];
  } else {
    return "";
  }
}

Arguments::Arguments(const Arguments &x)
    : args(x.args), index_arg(x.index_arg) {}

int Arguments::size() { return args.size(); }

Worker::Worker(const Arguments &a) : args(a) {}

std::string ReadFileWorker::process(std::string) {
  std::string filename = args.get_argument();
  std::ifstream fin;
  fin.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try {
    fin.open(filename);
  } catch (std::ifstream::failure) {
    throw WorkerException("readfile worker cannot open file \"" + filename +
                          "\"");
  }
  fin.seekg(0, fin.end);
  int size = fin.tellg();
  fin.seekg(0, fin.beg);

  std::string s(size, '\0');

  fin.read(&s[0], size);
  fin.close();
  return s;
}

WorkerType ReadFileWorker::type() { return WorkerType::READFILE; }

std::string WriteFileWorker::process(std::string text) {
  std::string filename = args.get_argument();
  std::ofstream fout;
  fout.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try {
    fout.open(filename);
  } catch (std::ifstream::failure) {
    throw WorkerException("write worker cannot open file \"" + filename + "\"");
  }
  fout << text;
  fout.close();
  return "";
}

WorkerType WriteFileWorker::type() { return WorkerType::WRITEFILE; }

std::string GrepWorker::process(std::string text) {
  std::string word = args.get_argument();
  std::string line;
  std::string result;
  for (char symbol : text) {
    if (symbol != '\n') {
      line += symbol;
    } else if (line.find(word) != std::string::npos) {
      result += line + '\n';
      line = "";
    } else {
      line = "";
    }
  }
  if (line.find(word) != std::string::npos) {
    result += line;
  }
  return result;
}

WorkerType GrepWorker::type() { return WorkerType::GREP; }

std::string SortWorker::process(std::string text) {
  std::string line;
  std::vector<std::string> v;
  for (char symbol : text) {
    if (symbol != '\n') {
      line += symbol;
    } else {
      v.push_back(line);
      line = "";
    }
  }
  if (!line.empty()) {
    v.push_back(line);
  }
  std::sort(v.begin(), v.end());
  std::string result;
  for (size_t i = 0; i < v.size(); ++i) {
    if (i == 0) {
      result += v[i];
    } else {
      result += '\n' + v[i];
    }
  }
  return result;
}

WorkerType SortWorker::type() { return WorkerType::SORT; }

std::string ReplaceWorker::process(std::string text) {
  std::string word1 = args.get_argument();
  if (word1.empty()) {
    return text;
  }
  std::string word2 = args.get_argument();
  for (auto position = text.find(word1); position != std::string::npos;
       position = text.find(word1, position)) {
    text.replace(position, word1.length(), word2);
    position += word2.length();
  }
  return text;
}

WorkerType ReplaceWorker::type() { return WorkerType::REPLACE; }

std::string DumpWorker::process(std::string text) {
  std::string filename = args.get_argument();
  std::ofstream fout;
  fout.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try {
    fout.open(filename);
  } catch (std::ifstream::failure) {
    throw WorkerException("dump worker cannot open file \"" + filename + "\"");
  }
  fout << text;
  fout.close();
  return text;
}

WorkerType DumpWorker::type() { return WorkerType::DUMP; }
