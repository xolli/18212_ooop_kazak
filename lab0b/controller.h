#ifndef LAB0B_CONTROLLER_H
#define LAB0B_CONTROLLER_H

#include <string> // std::string
#include <list> // std::list
#include <vector> //std::vector
#include <map> // std::map
#include <fstream>

class Controller {
    std::string output_format;
    std::string input_filename;
    std::string output_filename;
public:
    Controller(const std::string &out, const std::string &file_input, const std::string &file_output);

    bool doIt();
};

class Reader {
    std::ifstream fin;
public:
    Reader(const std::string &file_name);

    ~Reader();

    std::string read();

    bool end();
};

class Storage {
    std::map<std::string, std::pair<long long, long double>> stat;
    long long count;
public:
    Storage();

    void add(const std::string &new_str);

    std::vector<std::tuple<long long, long double, std::string>> get();
};

class Reporter {
    std::ofstream fout;
    std::string format;
    std::vector<std::tuple<long long, long double, std::string>> stat;
public:
    Reporter(std::string &name_file, std::string &out_format,
             std::vector<std::tuple<long long, long double, std::string>> statistics);

    ~Reporter();

    void report();
};

#endif //LAB0B_CONTROLLER_H
