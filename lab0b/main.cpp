#include "controller.h"

int main(int argc, char *argv[]) {
    if (argc < 3) {
        return 1;
    }

    Controller main("csv", argv[1], argv[2]);
    main.doIt();

    return 0;
}
