#include "controller.h"
#include <fstream> // std::ifstream, std::ofstream
#include <algorithm> // sort
#include <utility> // pair

std::string Reader::read() {
    std::string new_str;
    char ch;
    if (!fin.eof()) {
        ch = fin.get();
        while ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9')) {
            new_str += ch;
            ch = fin.get();
        }
    } else {
        return "";
    }
    return new_str;
}

bool Reader::end() {
    return fin.eof();
}

Reader::Reader(const std::string &file_name) {
    fin.open(file_name);
}

Reader::~Reader() {
    fin.close();
}

Storage::Storage() {
    count = 0;
}

void Storage::add(const std::string &new_str) {
    ++count;
    if (stat.find(new_str) == stat.end()) {
        stat[new_str] = std::make_pair(0, 0.0);
    }
    ++stat[new_str].first;
    stat[new_str].second = stat[new_str].first / (long double) count;
}

std::vector<std::tuple<long long, long double, std::string>> Storage::get() {
    std::vector<std::tuple<long long, long double, std::string>> result;
    for (auto &it : stat) {
        result.emplace_back(std::make_tuple(it.second.first, it.second.second, it.first));
    }
    std::sort(result.rbegin(), result.rend());
    return result;
}

Reporter::Reporter(std::string &name_file, std::string &out_format,
                   std::vector<std::tuple<long long, long double, std::string>> statistics) {
    fout.open(name_file);
    format = out_format;
    stat = statistics;
}

Reporter::~Reporter() {
    fout.close();
}

void Reporter::report() {
    for (auto it = stat.begin(); it != stat.end(); ++it) {
        fout << std::get<2>(*it) << ',' << std::get<0>(*it) << ',' << std::get<1>(*it) * 100 << '\n';
    }
}

Controller::Controller(const std::string &out, const std::string &file_input, const std::string &file_output) {
    output_format = out;
    input_filename = file_input;
    output_filename = file_output;
}

bool Controller::doIt() {
    Reader read(input_filename);
    Storage statistics;
    while (!read.end()) {
        std::string str = read.read();
        statistics.add(str);
    }
    auto stat = statistics.get();

    Reporter report(output_filename, output_format, stat);
    report.report();
    return true;
}
