cmake_minimum_required(VERSION 3.14)
project(lab1)

set(CMAKE_CXX_STANDARD 14)

find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

add_executable(runTests test_main.cpp TritSet.cpp TritSet.h test_main.cpp)
target_link_libraries(runTests ${GTEST_LIBRARIES} pthread)
