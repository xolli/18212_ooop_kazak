#include "TritSet.h"
#include <iostream>

TritSet::TritSet(uint reserve) {
    data = new uint[reserve];
    for (uint i = 0; i < reserve; ++i) {
        data[i] = Unknown;
    }
    size_data = reserve;
}

TritSet::TritSet(const TritSet &trits) {
    data = new uint[trits.size_data];
    for (size_t i = 0; i < trits.size_data; ++i) {
        data[i] = trits.data[i];
    }
    size_data = trits.size_data;
}

size_t TritSet::capacity() const {
    return size_data;
}

TritSet::~TritSet() {
    delete[] data;
}

TritSet::reference::reference(TritSet &data, size_t ind) : ptr(&data), index(ind) {}

TritSet::reference::reference(const reference &x) {
    ptr = x.ptr;
    index = x.index;
}

TritSet::reference &TritSet::reference::operator=(uint val) {
    if (index < ptr->capacity()) {
        ptr->data[index] = val;
    } else if (val != Unknown) {
        uint *tmp = new uint[index + 1];
        for (size_t i = 0; i < ptr->capacity(); ++i) {
            tmp[i] = ptr->data[i];
        }
        for (size_t i = ptr->capacity(); i < index; ++i) {
            tmp[i] = Unknown;
        }
        tmp[index] = val;
        delete[] ptr->data;
        ptr->data = tmp;
        ptr->size_data = index + 1;
    }
    return *this;
}

TritSet::reference &TritSet::reference::operator=(TritSet::reference val) {
    if (val.index >= val.ptr->capacity()) {
        *this = Unknown;
    } else {
        *this = val.ptr->data[val.index];
    }
    return *this;
}

TritSet::reference TritSet::operator[](uint const &index) {
    return reference(*this, index);
}

void TritSet::shrink() {
    size_t len = length();
    uint *tmp = new uint[len];
    for (size_t i = 0; i < len; ++i) {
        tmp[i] = data[i];
    }
    delete[] data;
    data = tmp;
    size_data = len;
}

TritSet TritSet::operator!() const {
    TritSet result(capacity());
    for (size_t i = 0; i < result.capacity(); ++i) {
        result[i] = (data[i] == True) ? False : Unknown;
        result[i] = (data[i] == False) ? True : Unknown;
    }
    return result;
}

size_t TritSet::length() {
    size_t result = UINTMAX_MAX;
    for (size_t i = 0; i < capacity(); ++i) {
        result = (data[i] != Unknown) ? i : result;
    }
    if (result != UINTMAX_MAX) {
        return result + 1;
    } else {
        return 0;
    }
}

void TritSet::trim(size_t last_index) {
    for (size_t i = last_index; i < capacity(); ++i) {
        (*this)[i] = Unknown;
    }
}

size_t TritSet::cardinality(Trit value) {
    size_t result = 0;
    for (size_t i = 0; i < size_data; ++i) {
        result += (data[i] == value) ? 1 : 0;
    }
    if (value != Unknown) {
        return result;

    } else {
        return result - (size_data - length());
    }
}

std::unordered_map<Trit, size_t, std::hash<int> > TritSet::cardinality() {
    std::unordered_map<Trit, size_t, std::hash<int> > result;
    result[True] = cardinality(True);
    result[False] = cardinality(False);
    result[Unknown] = cardinality(Unknown);
    return result;
}

TritSet operator&(TritSet &left, TritSet &right) {
    TritSet result(std::max(left.capacity(), right.capacity()));
    for (size_t i = 0; i < result.capacity(); ++i) {
        if (left[i] == True && right[i] == True) {
            result[i] = True;
        } else if (left[i] == False || right[i] == False) {
            result[i] = False;
        }
    }
    return result;
}

TritSet operator|(TritSet &left, TritSet &right) {
    TritSet result(std::max(left.capacity(), right.capacity()));
    for (size_t i = 0; i < result.capacity(); ++i) {
        if (left[i] == True || right[i] == True) {
            result[i] = True;
        } else if (left[i] == False && right[i] == False) {
            result[i] = False;
        }
    }
    return result;
}

bool operator==(const TritSet::reference &left, const uint &right) {
    if (left.index < left.ptr->capacity()) {
        return left.ptr->data[left.index] == right;
    } else {
        return Unknown == right;
    }
}

bool operator==(const uint &left, const TritSet::reference &right) {
    return right == left;
}

bool operator==(const TritSet::reference &left, const TritSet::reference &right) {
    uint a = 0, b = 0;
    if (left.index < left.ptr->capacity()) {
        a = left.ptr->data[left.index];
    } else {
        a = Unknown;
    }
    if (right.index < right.ptr->capacity()) {
        b = right.ptr->data[right.index];
    } else {
        b = Unknown;
    }
    return a == b;
}

std::ostream &operator<<(std::ostream &os, const TritSet::reference &x) {
    if ((*(x.ptr))[x.index] == Unknown) {
        os << "Unknown";
    } else if ((*x.ptr)[x.index] == False) {
        os << "False";
    } else {
        os << "True";
    }
    return os;
}
