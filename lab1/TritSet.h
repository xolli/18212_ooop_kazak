#ifndef LAB1_TRITSET_H
#define LAB1_TRITSET_H

#include <unordered_map>

enum Trit {
    False, Unknown, True
};

class TritSet {
    uint *data;
    size_t size_data;
public:
    explicit TritSet(uint reserve);

    TritSet(const TritSet &trits);

    size_t capacity() const;

    ~TritSet();

    class reference {
        TritSet *ptr;
        size_t index;
    public:
        reference(TritSet &data, size_t ind);

        reference(const reference &x);

        reference &operator=(uint val);

        reference &operator=(reference val);

        friend bool operator==(const reference &left, const uint &right);

        friend bool operator==(const uint &left, const reference &right);

        friend bool operator==(const reference &left, const reference &right);

        friend std::ostream& operator<< (std::ostream& os, const reference& x);

    };

    friend bool operator==(const reference &left, const uint &right);

    friend bool operator==(const uint &left, const reference &right);

    friend bool operator==(const reference &left, const reference &right);

    friend std::ostream& operator<< (std::ostream& os, const reference& x);

    reference operator[](uint const &index);

    void shrink();

    TritSet operator!() const;

    size_t length();

    void trim(size_t last_index);

    size_t cardinality(Trit value);

    std::unordered_map<Trit, size_t, std::hash<int> > cardinality();

    friend TritSet operator&(TritSet &left, TritSet &right);

    friend TritSet operator|(TritSet &left, TritSet &right);


};

#endif //LAB1_TRITSET_H
