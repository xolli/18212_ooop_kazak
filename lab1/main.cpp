#include <iostream>
#include "TritSet.h"

int main() {
    TritSet setA(1000);
    TritSet setB(2000);
    TritSet setC = !setB;
    std::cout << (setC.capacity() == setB.capacity()) << std::endl;

    return 0;
}
