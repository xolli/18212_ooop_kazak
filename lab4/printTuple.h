#ifndef LAB4_PRINTTUPLE_H
#define LAB4_PRINTTUPLE_H

#include <iostream>
#include <tuple>

template <class Ch, class Tr, typename Type, unsigned BEGIN, unsigned END>
struct tuple_writer {
  static void read(std::basic_ostream<Ch, Tr> &out, const Type &value) {
    out << std::get<BEGIN>(value) << ", ";
    tuple_writer<Ch, Tr, Type, BEGIN + 1, END>::read(out, value);
  }
};

template <class Ch, class Tr, typename Type, unsigned N>
struct tuple_writer<Ch, Tr, Type, N, N> {
  static void read(std::basic_ostream<Ch, Tr> &out, const Type &value) {
    out << std::get<N>(value);
  }
};

template <class Ch, class Tr, typename... Types>
std::basic_ostream<Ch, Tr> &operator<<(std::basic_ostream<Ch, Tr> &out,
                                       const std::tuple<Types...> &value) {
  tuple_writer<Ch, Tr, std::tuple<Types...>, 0, sizeof...(Types) - 1>::read(out, value);
  return out;
}

#endif // LAB4_PRINTTUPLE_H
