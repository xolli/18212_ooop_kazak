#ifndef LAB4_CSVITERATOR_H
#define LAB4_CSVITERATOR_H

template <typename... Args> class CSVParser;

#include "CSVParser.h"
#include "State.h"
#include "inputTuple.h"

#include <fstream>
#include <iterator>
#include <memory>
#include <sstream>

template <typename... Args>
class CSVIterator : public std::iterator<std::input_iterator_tag, std::tuple<Args...>> {
  std::ifstream &fin;
  State state;
  std::shared_ptr<std::tuple<Args...>> p;
  CSVIterator(std::ifstream &file);
  CSVIterator(State state, std::ifstream &file);

public:
  CSVIterator(const CSVIterator &it);
  bool operator!=(CSVIterator const &other) const;
  bool operator==(CSVIterator const &other) const; // need for BOOST_FOREACH
  typename CSVIterator::reference operator*() const;
  CSVIterator &operator++();
  friend class CSVParser<Args...>;
};

template <typename... Args>
CSVIterator<Args...>::CSVIterator(std::ifstream &file) : fin(file) {
  p = std::make_shared<std::tuple<Args...>>();
  state = State::READ;
  ++*this;
}

template <typename... Args>
CSVIterator<Args...>::CSVIterator(const CSVIterator &it) : p(it.p) {}

template <typename... Args>
bool CSVIterator<Args...>::operator!=(CSVIterator const &other) const {
  return state != other.state;
}

template <typename... Args>
bool CSVIterator<Args...>::operator==(CSVIterator const &other) const {
  return state == other.state;
}

template <typename... Args>
typename CSVIterator<Args...>::reference CSVIterator<Args...>::operator*() const {
  return *p;
}

template <typename... Args> CSVIterator<Args...> &CSVIterator<Args...>::operator++() {
  if (fin.eof() || state == State::END) {
    state = State::END;
    return *this;
  }
  std::string buffer;
  getline(fin, buffer);
  std::stringstream ss;
  ss << buffer;
  ss >> *p;
  return *this;
}

template <typename... Args>
CSVIterator<Args...>::CSVIterator(State state, std::ifstream &file)
    : state(state), fin(file) {}

#endif // LAB4_CSVITERATOR_H
