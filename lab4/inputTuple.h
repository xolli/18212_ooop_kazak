#ifndef LAB4_INPUTTUPLE_H
#define LAB4_INPUTTUPLE_H

#include <fstream>
#include <sstream>
#include <tuple>

template <class T> T convert(std::string &s) {
  T t;
  std::stringstream ss(s);
  ss >> t;
  return T(t);
}

template <> std::string convert(std::string &s) {
  for (auto position = s.find("\","); position != std::string::npos;
       position = s.find("\",", position)) {
    s.replace(position, 2, ",");
    position += 1;
  }
  return s;
}

template <class Ch, class Tr, typename Type, unsigned BEGIN, unsigned END>
struct tuple_reader {
  static void read(std::basic_istream<Ch, Tr> &input, Type &value) {
    std::string buffer;
    // int posComma = 0;
    char charPrev = '\0';
    char charCur = input.get();
    while (!(charCur == ',' && charPrev != '"')) {
      buffer += charCur;
      charPrev = charCur;
      charCur = input.get();
      //++posComma;
    }
    // std::getline(input, buffer, ',');
    std::get<BEGIN>(value) = convert<typeof(std::get<BEGIN>(value))>(buffer);
    tuple_reader<Ch, Tr, Type, BEGIN + 1, END>::read(input, value);
  }
};

template <class Ch, class Tr, typename Type, unsigned N>
struct tuple_reader<Ch, Tr, Type, N, N> {
  static void read(std::basic_istream<Ch, Tr> &input, Type &value) {
    std::string buffer;
    std::getline(input, buffer, ',');
    std::get<N>(value) = convert<typeof(std::get<N>(value))>(buffer);
  }
};

template <class Ch, class Tr, typename... Types>
std::basic_istream<Ch, Tr> &operator>>(std::basic_istream<Ch, Tr> &input,
                                       std::tuple<Types...> &value) {
  tuple_reader<Ch, Tr, std::tuple<Types...>, 0, sizeof...(Types) - 1>::read(input, value);
  return input;
}

#endif // LAB4_INPUTTUPLE_H
