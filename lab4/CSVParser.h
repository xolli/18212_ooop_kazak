#ifndef LAB4_CSVPARSER_H
#define LAB4_CSVPARSER_H

template <typename... Args> class CSVIterator;

#include "CSVIterator.h"
#include "State.h"
#include <fstream>

template <typename... Args> class CSVParser {
  int skip;
  int fileSize;
  std::ifstream &fin;

public:
  typedef CSVIterator<Args...> iterator;
  iterator begin();
  iterator end();

  CSVParser(std::ifstream &file, int skipLines);
  ~CSVParser();
};

template <typename... Args>
CSVParser<Args...>::CSVParser(std::ifstream &file, int skipLines)
    : skip(skipLines), fin(file) {
  int curPos = fin.tellg();
  fin.seekg(0, std::ios::end);
  fileSize = file.tellg();
  fin.seekg(curPos, std::ios::beg);
  for (int i = 0; i < skip; ++i) {
    std::string s;
    getline(fin, s);
  }
}

template <typename... Args> CSVParser<Args...>::~CSVParser() {}

template <typename... Args>
typename CSVParser<Args...>::iterator CSVParser<Args...>::begin() {
  return iterator(fin);
}
template <typename... Args>
typename CSVParser<Args...>::iterator CSVParser<Args...>::end() {
  return CSVParser::iterator(State::END, fin);
}

#endif // LAB4_CSVPARSER_H
