#ifndef LAB4_STATE_H
#define LAB4_STATE_H

enum class State { READ, END };

#endif // LAB4_STATE_H
