#include <fstream>
#include <iostream>

#include "CSVParser.h"
#include "printTuple.h"

int main() {
  std::ifstream file("test.csv");
  CSVParser<int, std::string, int> parser(file, 0);

  for (auto rs : parser) {
    std::cout << rs << std::endl;
  }

  return 0;
}
