#ifndef LAB3_GAMECONTROL_H
#define LAB3_GAMECONTROL_H

class HumanGamer;
class Gamer;

#include "Gamer/Gamer.h"

#include "GameView.h"
#include <string>
#include <utility>

class GameControl {
public:
  virtual Ship getShip() = 0;
  virtual std::pair<int, int> getCoordStrike(HumanGamer &player) = 0;
  virtual std::string getPlayerName() = 0;
};

class ConsoleControl : public GameControl {
public:
  Ship getShip() override;
  std::pair<int, int> getCoordStrike(HumanGamer &player) override;
  std::string getPlayerName() override;
};

#endif // LAB3_GAMECONTROL_H
