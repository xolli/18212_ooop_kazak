#ifndef LAB3_MYEXCEPTIONS_H
#define LAB3_MYEXCEPTIONS_H

#include <exception>
#include <string>

class BoardCreate : public std::exception {
public:
  [[nodiscard]] const char *what() const noexcept override {
    return "Incorrect board's width or height";
  };
};

class OutOfTheBoard : public std::exception {
public:
  [[nodiscard]] const char *what() const noexcept override {
    return "Incorrect ship's coord";
  };
};

class ShipsCross : public std::exception {
public:
  [[nodiscard]] const char *what() const noexcept override { return "Ships cross"; };
};

class PlayerCreate : public std::exception {
public:
  [[nodiscard]] const char *what() const noexcept override {
    return "Player can't create";
  };
};

class IncorrectInput : public std::exception {
public:
  [[nodiscard]] const char *what() const noexcept override { return "Incorrect input"; };
};

class ManyShips : public std::exception {
public:
  [[nodiscard]] const char *what() const noexcept override { return "Many replays ship's length"; };
};

#endif // LAB3_MYEXCEPTIONS_H
