#ifndef LAB3_GAMEVIEW_H
#define LAB3_GAMEVIEW_H

class Gamer;

#include "Board.h"
#include "Gamer/Gamer.h"
#include <string>

class GameView {
public:
  virtual void ShowState(Gamer *player, Strike lastStrike, int numberRound = 1) = 0;
  virtual void showResultRound(std::pair<Gamer *, Gamer *> players) = 0;
  virtual void tryAgain(std::string input) = 0;
  virtual void pause() = 0;
};

class ConsoleView : public GameView {
private:
  void showEnemyBoard(Gamer *player);
  void showPlayerBoard(Gamer *player);

public:
  void ShowState(Gamer *player, Strike lastStrike, int numberRound = 1) override;
  void showResultRound(std::pair<Gamer *, Gamer *> players) override;
  void tryAgain(std::string input) override;
  void pause();
};

#endif // LAB3_GAMEVIEW_H
