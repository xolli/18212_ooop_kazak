#ifndef LAB3_UTILITY_H
#define LAB3_UTILITY_H

#include <cstdlib>

static int minimum(int a, int b) { return a < b ? a : b; }
static int maximum(int a, int b) { return a > b ? a : b; }
static void clear_terminal() {
#if defined(_WIN32)
  system("CLS");
#elif defined(__gnu_linux__)
  system("clear");
#endif
}

#endif // LAB3_UTILITY_H
