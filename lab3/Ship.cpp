#include "Ship.h"

State Ship::get_state() { return state; }

void Ship::change_state(State new_state) { state = new_state; }

Direction Ship::get_direction() { return direction; }

int Ship::get_length() { return length; }

Ship::Ship(std::pair<int, int> set_coord, int set_length, Direction set_direction) {
  coord = set_coord;
  length = set_length;
  direction = set_direction;
  state = State::WHOLE;
  count_shot = 0;
}

std::pair<int, int> Ship::get_coord() { return coord; }
bool Ship::hit(std::pair<int, int> shotCoord) {
  if ((direction == Direction::RIGHT && coord.second == shotCoord.second &&
       coord.first <= shotCoord.first && shotCoord.first < coord.first + length) ||
      (direction == Direction::UP && coord.first == shotCoord.first &&
       coord.second <= shotCoord.second && shotCoord.second < coord.second + length)) {
    if (++count_shot >= length) {
      change_state(State::KILLED);
    } else {
      change_state(State::DAMAGED);
    }
    return true;
  }
  return false;
}

bool Ship::overlap(Ship other) {
  std::pair<int, int> downLeft = area().first;
  std::pair<int, int> upperRight = area().second;

  if (other.get_direction() == Direction::RIGHT &&
      other.get_coord().second >= downLeft.second &&
      other.get_coord().second <= upperRight.second &&
      (other.get_coord().first <= upperRight.first &&
       other.get_coord().first + other.get_length() - 1 >= downLeft.first)) {
    return true;
  } else if (other.get_direction() == Direction::UP &&
             other.get_coord().first >= downLeft.first &&
             other.get_coord().first <= upperRight.first &&
             other.get_coord().second <= upperRight.second &&
             (other.get_coord().second + other.get_length() - 1) >= downLeft.second) {
    return true;
  }
  return false;
}

std::pair<std::pair<int, int>, std::pair<int, int>> Ship::area() {
  std::pair<std::pair<int, int>, std::pair<int, int>> result;
  if (direction == Direction::RIGHT) {
    return std::make_pair(std::make_pair(coord.first - 1, coord.second - 1),
                          std::make_pair(coord.first + length, coord.second + 1));
  } else {
    return std::make_pair(std::make_pair(coord.first - 1, coord.second - 1),
                          std::make_pair(coord.first + 1, coord.second + length));
  }
}
