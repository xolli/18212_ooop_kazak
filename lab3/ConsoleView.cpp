#include <iostream>

#include "GameView.h"
#include "utility.h"


void ConsoleView::ShowState(Gamer *player, Strike lastStrike, int numberRound) {
  clear_terminal();
  std::cout << "Player: " << player->getName() << std::endl;
  if (lastStrike == Strike::KILL) {
    std::cout << "You KILL the ship! =)" << std::endl;
  } else if (lastStrike == Strike::HIT) {
    std::cout << "You INJURE the ship! =)" << std::endl;
  } else if (lastStrike == Strike::MISS) {
    std::cout << "You MISS =(" << std::endl;
  } else {
    std::cout << "Welcome to the \"Sea battle\", Round " << numberRound << std::endl;
  }
  std::cout << "My Board:" << std::endl;
  showPlayerBoard(player);
  std::cout << "Enemy Board:" << std::endl;
  showEnemyBoard(player);
}
void ConsoleView::showResultRound(std::pair<Gamer *, Gamer *> players) {
  clear_terminal();
  if (players.first->lose()) {
    std::cout << "Player 1 LOSE";
  } else {
    std::cout << "Player 2 LOSE";
  }
  std::cout << std::endl;
  std::cout << "Player 1:" << std::endl;
  showPlayerBoard(players.first);
  std::cout << "Player 2:" << std::endl;
  showPlayerBoard(players.second);
}
void ConsoleView::showPlayerBoard(Gamer *player) {
  std::cout << "   ";
  for (char i = 'a'; i < 'a' + WIDTH; ++i) {
    std::cout << i;
  }
  std::cout << std::endl;
  auto board = (player->myBoard).get_board();
  for (int y = HEIGHT - 1; y >= 0; --y) {
    std::cout << y << "| ";
    for (int x = 0; x < WIDTH; ++x) {
      if (board[x][y] == CellState::WHOLEEMPTY) {
        std::cout << ".";
      } else if (board[x][y] == CellState::SHOTEMPTY) {
        std::cout << "0";
      } else if (board[x][y] == CellState::WHOLESHIP) {
        std::cout << "#";
      } else if (board[x][y] == CellState::SHOTSHIP) {
        std::cout << "*";
      }
    }
    std::cout << std::endl;
  }
}
void ConsoleView::showEnemyBoard(Gamer *player) {
  std::cout << "   ";
  for (char i = 'a'; i < 'a' + WIDTH; ++i) {
    std::cout << i;
  }
  std::cout << std::endl;
  for (int y = HEIGHT - 1; y >= 0; --y) {
    std::cout << y << "| ";
    for (int x = 0; x < WIDTH; ++x) {
      if (player->enemyBoard[x][y] == EnemyCell::SHIP) {
        std::cout << "*";
      } else if (player->enemyBoard[x][y] == EnemyCell::EMPTY) {
        std::cout << "0";
      } else {
        std::cout << ".";
      }
    }
    std::cout << std::endl;
  }
}
void ConsoleView::tryAgain(std::string input) {
  std::cout << input << ", try again" << std::endl;
}
void ConsoleView::pause() {
  std::string s;
  getline(std::cin, s);
}
