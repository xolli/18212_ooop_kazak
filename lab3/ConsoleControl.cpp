#include "GameControl.h"
#include "MyExceptions.h"
#include "utility.h"

#include <iostream>
#include <sstream>

Ship ConsoleControl::getShip() {
  std::cout << "Input ship length, x coord (letter), y coord(numeral), direction: ";
  std::string line;
  std::getline(std::cin, line);

  int length = -1;
  char x = '\0';
  int y = -1;
  char direction = '\0';

  std::istringstream stream(line);
  stream >> length >> x >> y >> direction;
  if (stream.fail()) {
    throw IncorrectInput();
  }
  while (!stream.eof() && direction != 'r' && direction != 'u') {
    stream >> direction;
  }
  if (direction != 'r' && direction != 'u') {
    throw IncorrectInput();
  }
  Direction d = (direction == 'r') ? Direction::RIGHT : Direction::UP;
  return Ship(std::pair{x - 'a', y}, length, d);
}
std::pair<int, int> ConsoleControl::getCoordStrike(HumanGamer &player) {
  std::cout << "Enter the coordinates where you will shoot: ";
  char x = ' ';
  int y = -1;
  std::string line;
  std::getline(std::cin, line);
  std::istringstream stream(line);
  while (x == ' ' && !stream.fail()) {
    stream >> x;
  }
  stream >> y;

  while (stream.fail() || x < 'a' || x >= 'a' + WIDTH || y < 0 || y > HEIGHT ||
         player.enemyBoard[x - 'a'][y] != EnemyCell::UNKNOWN) {
    std::cout << "Incorrect input, try again" << std::endl;
    std::cout << "Enter the coordinates where you will shoot: ";
    std::getline(std::cin, line);
    stream = std::istringstream(line);
    x = ' ';
    while (x == ' ' && !stream.fail()) {
      stream >> x;
    }
    stream >> y;
  }
  return std::pair{x - 'a', y};
}
std::string ConsoleControl::getPlayerName() {
  std::cout << "Input your name: ";
  std::string result;
  getline(std::cin, result);
  return result;
}
