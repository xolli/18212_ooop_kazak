#include "Game.h"
#include "optionparser.h"
#include <iostream>
#include <string>

enum optionIndex { UNKNOWN, HELP, COUNT, FIRST, SECOND };

const option::Descriptor usage[] = {
    {UNKNOWN, 0, "", "", option::Arg::None,
     "USAGE: example [options]\n\n"
     "Options:"},
    {HELP, 0, "h", "help", option::Arg::None, " --help -  h \tPrint usage and exit."},
    {COUNT, 0, "c", "count", option::Arg::Optional, " --count, -c  \tCount rounds."},
    {FIRST, 0, "f", "first", option::Arg::Optional, " --first, -f \t Type first player."},
    {SECOND, 0, "s", "second", option::Arg::Optional,
     " --second, -s \t Type second player."},
    {UNKNOWN, 0, "", "", option::Arg::None,
     "\nExamples:\n"
     "  ./lab3 --first=player --second=optimal\n"
     "  ./lab3 -f=player -s=random\n"},
    {0, 0, 0, 0, 0, 0}};

int main(int argc, char *argv[]) {
  // freopen("in.txt", "r", stdin);
  argc -= (argc > 0);
  argv += (argc > 0); // skip program name argv[0] if present
  option::Stats stats(usage, argc, argv);
  std::vector<option::Option> options(stats.options_max);
  std::vector<option::Option> buffer(stats.buffer_max);
  option::Parser parse(usage, argc, argv, &options[0], &buffer[0]);
  if (parse.error()) {
    return 1;
  }
  if (options[HELP]) {
    option::printUsage(std::cout, usage);
    return 0;
  }

  std::string first = "random", second = "random";
  int count = 1;
  for (int i = 0; i < parse.optionsCount(); ++i) {
    option::Option &opt = buffer[i];
    if (opt.arg != NULL) {
      switch (opt.index()) {
      case FIRST:
        first = opt.arg;
        break;
      case SECOND:
        second = opt.arg;
        break;
      case COUNT:
        count = std::atoi(opt.arg);
        break;
      }
    }
  }
  ViewMode viewMode;
  GamerType firstType;
  if (first == "random") {
    firstType = GamerType::RANDOM;
  } else if (first == "optimal") {
    firstType = GamerType::OPTIMAL;
  } else if (first == "player") {
    firstType = GamerType::CONSOLE;
  }
  else {
    std::cerr << "Incorrect first player" << std::endl;
    return 1;
  }
  GamerType secondType;
  if (second == "random") {
    secondType = GamerType::RANDOM;
  } else if (second == "optimal") {
    secondType = GamerType::OPTIMAL;
  } else if (second == "player"){
    secondType = GamerType::CONSOLE;
  }
  else {
    std::cerr << "Incorrect second player" << std::endl;
    return 1;
  }
  if (firstType != GamerType::CONSOLE && secondType != GamerType::CONSOLE) {
    viewMode = ViewMode::ALLPLAYER;
  }
  else {
    viewMode = ViewMode::NOBODY;
  }

  Game main(count, firstType, secondType, WIDTH, HEIGHT, viewMode);
  main.mainloop();
  return 0;
}
