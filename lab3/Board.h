#ifndef LAB3_BOARD_H
#define LAB3_BOARD_H

#include <vector>

#define WIDTH 10
#define HEIGHT 10

#include "Ship.h"

enum class Strike { KILL, HIT, MISS, REPLAY, UNKNOWN };

enum class CellState { SHOTSHIP, SHOTEMPTY, WHOLESHIP, WHOLEEMPTY };

enum class EnemyCell { SHIP, EMPTY, UNKNOWN };

class Board {
  std::vector<std::vector<bool>> data{};
  unsigned int width{};
  unsigned int height{};
  std::vector<Ship> ships;

public:
  Board(int setWidth, int setHeight);
  Board() = default;
  void addShip(Ship added_ship);
  Strike shot(std::pair<int, int> coord);
  bool end();
  std::vector<std::vector<CellState>> get_board();
  int get_width();
  int get_height();
};

#endif // LAB3_BOARD_H
