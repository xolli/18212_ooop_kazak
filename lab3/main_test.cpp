#include "Board.h"
#include "MyExceptions.h"
#include "Ship.h"
#include <gtest/gtest.h>

TEST(BoardCreateTest, PositiveVal) { EXPECT_NO_THROW(Board(10, 10)); }

TEST(BoardCreateTest, NegativeVal_1) { EXPECT_THROW(Board(-10, -10), BoardCreate); }

TEST(BoardCreateTest, NegativeVal_2) { EXPECT_THROW(Board(0, -10), BoardCreate); }

TEST(BoardCreateTest, NegativeVal_3) { EXPECT_THROW(Board(-10, 0), BoardCreate); }

TEST(BoardCreateTest, NegativeVal_4) { EXPECT_THROW(Board(-10, 10), BoardCreate); }

TEST(BoardCreateTest, NegativeVal_5) { EXPECT_THROW(Board(10, -10), BoardCreate); }

TEST(BoardCreateTest, ZeroVal_1) { EXPECT_NO_THROW(Board(0, 0)); }

TEST(BoardCreateTest, ZeroVal_2) { EXPECT_NO_THROW(Board(0, 10)); }

TEST(BoardCreateTest, ZeroVal_3) { EXPECT_NO_THROW(Board(10, 0)); }

TEST(ShipHitTest, One_1) {
  Ship s(std::make_pair(0, 0), 1, Direction::RIGHT);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
}

TEST(ShipHitTest, One_2) {
  Ship s(std::make_pair(0, 0), 1, Direction::RIGHT);
  EXPECT_EQ(s.hit(std::pair{0, 1}), false);
}

TEST(ShipHitTest, One_3) {
  Ship s(std::make_pair(0, 0), 1, Direction::RIGHT);
  EXPECT_EQ(s.hit(std::pair{1, 0}), false);
}

TEST(ShipHitTest, Two_1) {
  Ship s(std::make_pair(0, 0), 2, Direction::RIGHT);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{1, 0}), true);
}

TEST(ShipHitTest, Two_2) {
  Ship s(std::make_pair(0, 0), 2, Direction::RIGHT);
  EXPECT_EQ(s.hit(std::pair{0, 1}), false);
  EXPECT_EQ(s.hit(std::pair{1, 1}), false);
}

TEST(ShipHitTest, Two_3) {
  Ship s(std::make_pair(0, 0), 2, Direction::RIGHT);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{1, 0}), true);
  EXPECT_EQ(s.hit(std::pair{1, 0}), true);
  EXPECT_EQ(s.hit(std::pair{1, 0}), true);
  EXPECT_EQ(s.hit(std::pair{1, 0}), true);
  EXPECT_EQ(s.hit(std::pair{1, 0}), true);
}

TEST(ShipHitTest, Two_4) {
  Ship s(std::make_pair(0, 0), 2, Direction::UP);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{0, 1}), true);
}

TEST(ShipHitTest, Three_1) {
  Ship s(std::make_pair(0, 0), 3, Direction::RIGHT);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{1, 0}), true);
  EXPECT_EQ(s.hit(std::pair{2, 0}), true);
}

TEST(ShipHitTest, Three_2) {
  Ship s(std::make_pair(0, 0), 3, Direction::UP);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{0, 1}), true);
  EXPECT_EQ(s.hit(std::pair{0, 2}), true);
}

TEST(ShipHitTest, Four_1) {
  Ship s(std::make_pair(0, 0), 4, Direction::UP);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{0, 1}), true);
  EXPECT_EQ(s.hit(std::pair{0, 2}), true);
  EXPECT_EQ(s.hit(std::pair{0, 3}), true);
}

TEST(ShipHitTest, Four_2) {
  Ship s(std::make_pair(0, 0), 4, Direction::RIGHT);
  EXPECT_EQ(s.hit(std::pair{0, 0}), true);
  EXPECT_EQ(s.hit(std::pair{1, 0}), true);
  EXPECT_EQ(s.hit(std::pair{2, 0}), true);
  EXPECT_EQ(s.hit(std::pair{3, 0}), true);
}

TEST(ShipStateTest, One) {
  Ship s(std::make_pair(0, 0), 1, Direction::RIGHT);
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{1, 1});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{0, 1});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{1, 0});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{0, 0});
  EXPECT_EQ(s.get_state(), State::KILLED);
}

TEST(ShipStateTest, Two) {
  Ship s(std::make_pair(0, 0), 2, Direction::RIGHT);
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{2, 0});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{2, 1});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{1, 1});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{0, 1});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{0, 0});
  EXPECT_EQ(s.get_state(), State::DAMAGED);
  s.hit(std::pair{1, 0});
  EXPECT_EQ(s.get_state(), State::KILLED);
}

TEST(ShipStateTest, Three) {
  Ship s(std::make_pair(5, 5), 3, Direction::RIGHT);
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{4, 5});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{8, 5});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{5, 6});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{6, 6});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{7, 6});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{5, 4});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{6, 4});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{7, 4});
  EXPECT_EQ(s.get_state(), State::WHOLE);
  s.hit(std::pair{5, 5});
  EXPECT_EQ(s.get_state(), State::DAMAGED);
  s.hit(std::pair{6, 5});
  EXPECT_EQ(s.get_state(), State::DAMAGED);
  s.hit(std::pair{7, 5});
  EXPECT_EQ(s.get_state(), State::KILLED);
}

TEST(ShipOverlapTest, simpleTest1) {
  Ship s1(std::make_pair(1, 1), 1, Direction::UP);
  Ship s2(std::make_pair(1, 1), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest2) {
  Ship s1(std::make_pair(1, 1), 1, Direction::RIGHT);
  Ship s2(std::make_pair(1, 1), 1, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest3) {
  Ship s1(std::make_pair(0, 0), 1, Direction::RIGHT);
  Ship s2(std::make_pair(0, 0), 1, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest4) {
  Ship s1(std::make_pair(0, 0), 1, Direction::UP);
  Ship s2(std::make_pair(0, 0), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest5) {
  Ship s1(std::make_pair(1, 1), 1, Direction::UP);
  Ship s2(std::make_pair(0, 0), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest6) {
  Ship s1(std::make_pair(1, 1), 1, Direction::UP);
  Ship s2(std::make_pair(1, 0), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest7) {
  Ship s1(std::make_pair(1, 1), 1, Direction::UP);
  Ship s2(std::make_pair(2, 0), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest8) {
  Ship s1(std::make_pair(1, 1), 1, Direction::UP);
  Ship s2(std::make_pair(2, 1), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest9) {
  Ship s1(std::make_pair(1, 1), 1, Direction::UP);
  Ship s2(std::make_pair(2, 2), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest10) {
  Ship s1(std::make_pair(1, 1), 1, Direction::UP);
  Ship s2(std::make_pair(1, 2), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest11) {
  Ship s1(std::make_pair(1, 1), 1, Direction::UP);
  Ship s2(std::make_pair(0, 2), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest12) {
  Ship s1(std::make_pair(1, 1), 1, Direction::UP);
  Ship s2(std::make_pair(0, 1), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest13) {
  Ship s1(std::make_pair(1, 1), 1, Direction::RIGHT);
  Ship s2(std::make_pair(0, 0), 1, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest14) {
  Ship s1(std::make_pair(1, 1), 1, Direction::RIGHT);
  Ship s2(std::make_pair(1, 0), 1, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest15) {
  Ship s1(std::make_pair(1, 1), 1, Direction::RIGHT);
  Ship s2(std::make_pair(2, 0), 1, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest16) {
  Ship s1(std::make_pair(1, 1), 1, Direction::RIGHT);
  Ship s2(std::make_pair(2, 1), 1, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest17) {
  Ship s1(std::make_pair(1, 1), 1, Direction::RIGHT);
  Ship s2(std::make_pair(2, 2), 1, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest18) {
  Ship s1(std::make_pair(1, 1), 1, Direction::RIGHT);
  Ship s2(std::make_pair(1, 2), 1, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest19) {
  Ship s1(std::make_pair(1, 1), 1, Direction::RIGHT);
  Ship s2(std::make_pair(0, 2), 1, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, simpleTest20) {
  Ship s1(std::make_pair(1, 1), 1, Direction::RIGHT);
  Ship s2(std::make_pair(0, 1), 1, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, TwoOneAngle) {
  Ship s1(std::make_pair(0, 0), 2, Direction::RIGHT);
  Ship s2(std::make_pair(2, 1), 1, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, TwoTwoAngleRight) {
  Ship s1(std::make_pair(0, 0), 2, Direction::RIGHT);
  Ship s2(std::make_pair(2, 1), 2, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, TwoTwoAngleUp) {
  Ship s1(std::make_pair(0, 0), 2, Direction::RIGHT);
  Ship s2(std::make_pair(2, 1), 2, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, TwoOneNoOverlap) {
  Ship s1(std::make_pair(0, 0), 2, Direction::RIGHT);
  Ship s2(std::make_pair(3, 1), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), false);
}

TEST(ShipOverlapTest, TwoTwoEq) {
  Ship s1(std::make_pair(0, 0), 2, Direction::RIGHT);
  Ship s2(std::make_pair(0, 0), 2, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, TwoTwoPerp) {
  Ship s1(std::make_pair(0, 0), 2, Direction::RIGHT);
  Ship s2(std::make_pair(0, 0), 2, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, TwoTwoAbove) {
  Ship s1(std::make_pair(0, 0), 2, Direction::RIGHT);
  Ship s2(std::make_pair(0, 1), 2, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, TwoTwoAbovePerp) {
  Ship s1(std::make_pair(0, 0), 2, Direction::RIGHT);
  Ship s2(std::make_pair(0, 1), 2, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, TwoTwoAboveNoOverlap) {
  Ship s1(std::make_pair(0, 0), 2, Direction::RIGHT);
  Ship s2(std::make_pair(0, 2), 2, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), false);
}

TEST(ShipOverlapTest, ThreeThreeOverlap) {
  Ship s1(std::make_pair(0, 0), 3, Direction::RIGHT);
  Ship s2(std::make_pair(1, 0), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, ThreeThreeOverlapEdge) {
  Ship s1(std::make_pair(0, 0), 2, Direction::RIGHT);
  Ship s2(std::make_pair(2, 0), 2, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, ThreeThreeTouch) {
  Ship s1(std::make_pair(0, 0), 3, Direction::RIGHT);
  Ship s2(std::make_pair(3, 0), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, ThreeThreeTouchAngle) {
  Ship s1(std::make_pair(0, 0), 3, Direction::RIGHT);
  Ship s2(std::make_pair(3, 1), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, ThreeThreeTouchAbove) {
  Ship s1(std::make_pair(0, 0), 3, Direction::RIGHT);
  Ship s2(std::make_pair(2, 1), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, ThreeThreeTouchAbove2) {
  Ship s1(std::make_pair(0, 0), 3, Direction::RIGHT);
  Ship s2(std::make_pair(1, 1), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, ThreeThreeTouchAboveUp) {
  Ship s1(std::make_pair(0, 0), 3, Direction::RIGHT);
  Ship s2(std::make_pair(1, 1), 3, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, ThreeThreeUpRightNoOverlap) {
  Ship s1(std::make_pair(0, 0), 3, Direction::UP);
  Ship s2(std::make_pair(2, 0), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), false);
}

TEST(ShipOverlapTest, ThreeThreeUpRightNoOverlap2) {
  Ship s1(std::make_pair(0, 0), 3, Direction::UP);
  Ship s2(std::make_pair(2, 1), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), false);
}

TEST(ShipOverlapTest, ThreeThreeUpRightNoOverlap3) {
  Ship s1(std::make_pair(0, 0), 3, Direction::UP);
  Ship s2(std::make_pair(2, 2), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), false);
}

TEST(ShipOverlapTest, ThreeThreeUpRightNoOverlap4) {
  Ship s1(std::make_pair(0, 0), 3, Direction::UP);
  Ship s2(std::make_pair(2, 3), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), false);
}

TEST(ShipOverlapTest, ThreeThreeUpRightNoOverlap5) {
  Ship s1(std::make_pair(0, 0), 3, Direction::UP);
  Ship s2(std::make_pair(1, 4), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), false);
}

TEST(ShipOverlapTest, ThreeThreeUpRightNoOverlap6) {
  Ship s1(std::make_pair(0, 0), 3, Direction::UP);
  Ship s2(std::make_pair(0, 4), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), false);
}

TEST(ShipOverlapTest, ThreeThreeUpRightTouch) {
  Ship s1(std::make_pair(0, 0), 3, Direction::UP);
  Ship s2(std::make_pair(0, 3), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, ThreeThreeUpRightEdge) {
  Ship s1(std::make_pair(0, 0), 3, Direction::UP);
  Ship s2(std::make_pair(0, 2), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, ThreeThreeUpRightOverlap) {
  Ship s1(std::make_pair(0, 0), 3, Direction::UP);
  Ship s2(std::make_pair(0, 1), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, ThreeThreeUpRightOverlap2) {
  Ship s1(std::make_pair(0, 0), 3, Direction::UP);
  Ship s2(std::make_pair(0, 0), 3, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, ThreeThreeUpUpOverlap) {
  Ship s1(std::make_pair(0, 0), 3, Direction::UP);
  Ship s2(std::make_pair(0, 3), 3, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, FourTwoOverlap) {
  Ship s1(std::make_pair(5, 5), 4, Direction::UP);
  Ship s2(std::make_pair(4, 4), 2, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, FourTwoOverlap2) {
  Ship s1(std::make_pair(5, 5), 4, Direction::UP);
  Ship s2(std::make_pair(3, 4), 2, Direction::RIGHT);
  EXPECT_EQ(s1.overlap(s2), true);
}

TEST(ShipOverlapTest, TwoOneOverlap) {
  Ship s1(std::make_pair(0, 4), 2, Direction::RIGHT);
  Ship s2(std::make_pair(0, 2), 1, Direction::UP);
  EXPECT_EQ(s1.overlap(s2), false);
}

TEST(BoardAddShipTest, Middle) {
  Board b(10, 10);
  EXPECT_NO_THROW(b.add_ship(Ship(std::pair{5, 5}, 1, Direction::RIGHT)));
}

TEST(BoardAddShipTest, Angle) {
  Board b(10, 10);
  EXPECT_NO_THROW(b.add_ship(Ship(std::pair{9, 9}, 1, Direction::RIGHT)));
}

TEST(BoardAddShipTest, IncorrectCoordX) {
  Board b(10, 10);
  EXPECT_THROW(b.add_ship(Ship(std::pair{30, 5}, 1, Direction::RIGHT)), OutOfTheBoard);
}

TEST(BoardAddShipTest, IncorrectCoordY) {
  Board b(10, 10);
  EXPECT_THROW(b.add_ship(Ship(std::pair{5, 30}, 1, Direction::RIGHT)), OutOfTheBoard);
}

TEST(BoardAddShipTest, IncorrectCoord) {
  Board b(10, 10);
  EXPECT_THROW(b.add_ship(Ship(std::pair{28, 30}, 1, Direction::RIGHT)), OutOfTheBoard);
}

TEST(BoardAddShipTest, BusyPlace) {
  Board b(10, 10);
  b.add_ship(Ship(std::pair{5, 5}, 4, Direction::RIGHT));
  EXPECT_THROW(b.add_ship(Ship(std::pair{5, 5}, 1, Direction::RIGHT)), ShipsCross);
}

TEST(BoardAddShipTest, BusyPlace2) {
  Board b(10, 10);
  b.add_ship(Ship(std::pair{5, 1}, 4, Direction::RIGHT));
  b.add_ship(Ship(std::pair{5, 3}, 4, Direction::RIGHT));
  b.add_ship(Ship(std::pair{5, 5}, 4, Direction::RIGHT));
  b.add_ship(Ship(std::pair{5, 7}, 4, Direction::RIGHT));
  b.add_ship(Ship(std::pair{5, 9}, 4, Direction::RIGHT));
  for (int k = 4; k <= 9; ++k) {
    for (int i = 0; i <= 10; ++i) {
      EXPECT_THROW(b.add_ship(Ship(std::pair{k, i}, 1, Direction::RIGHT)), ShipsCross);
    }
  }
}

TEST(BoardAddShipTest, NoBusyPlace) {
  Board b(10, 10);
  b.add_ship(Ship(std::pair{5, 5}, 4, Direction::RIGHT));
  for (int h = 0; h <= 10; h += 2) {
    for (int w = 0; w <= 3; w += 2) {
      EXPECT_NO_THROW(b.add_ship(Ship(std::pair{w, h}, 1, Direction::RIGHT)));
    }
  }
  for (int w = 5; w <= 9; w += 2) {
    EXPECT_NO_THROW(b.add_ship(Ship(std::pair{w, 3}, 1, Direction::RIGHT)));
  }
  for (int w = 5; w <= 9; w += 2) {
    EXPECT_NO_THROW(b.add_ship(Ship(std::pair{w, 7}, 1, Direction::RIGHT)));
  }
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}