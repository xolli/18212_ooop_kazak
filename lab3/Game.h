#ifndef LAB3_GAME_H
#define LAB3_GAME_H

#include "Gamer/GamerFactory.h"
#include "GameView.h"

enum class ViewMode { PLAYER1, PLAYER2, ALLPLAYER, NOBODY };

class Game {
  GamerFactory factory;
  Gamer *gamer1;
  Gamer *gamer2;
  std::pair<int, int> score;
  int numberRounds;
  GameView *view;
  ViewMode viewMode;

  void playRound();

public:
  Game(int countRounds, GamerType typeGamer1, GamerType typeGamer2, int width,
       int height, ViewMode mode);
  ~Game();
  void mainloop();
};

#endif // LAB3_GAME_H
