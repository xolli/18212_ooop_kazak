#ifndef LAB3_SHIP_H
#define LAB3_SHIP_H

#include <utility>

enum class Direction { RIGHT, UP };
enum class State { KILLED, DAMAGED, WHOLE, ERROR };

class Ship {
  std::pair<int, int> coord;
  int length;
  Direction direction;
  State state;
  int count_shot;
  void change_state(State new_state);
  std::pair<std::pair<int, int>, std::pair<int, int>>
  area(); // получить нижний левый и верхний угол занятой области
public:
  Ship(std::pair<int, int> set_coord, int set_length, Direction set_direction);
  State get_state();
  Direction get_direction();
  std::pair<int, int> get_coord();
  int get_length();
  bool hit(std::pair<int, int> shotCoord);
  bool overlap(Ship other);
};

#endif // LAB3_SHIP_H
