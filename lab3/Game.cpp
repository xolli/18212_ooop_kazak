#include "Game.h"

#include <fstream>
#include <iostream>

Game::~Game() {
  delete gamer1;
  delete gamer2;
  delete view;
}

void Game::playRound() {
  gamer1->arrangeShips();
  gamer2->arrangeShips();

  Strike lastStrike1 = Strike::UNKNOWN;
  Strike lastStrike2 = Strike::UNKNOWN;
  while (!gamer1->lose()) {
    lastStrike1 = Strike::UNKNOWN;
    do {
      if (viewMode == ViewMode::ALLPLAYER) {
        view->ShowState(gamer1, lastStrike1, score.first + score.second + 1);
        view->pause();
      }
    } while ((lastStrike1 = gamer1->move(gamer2)) != Strike::MISS && !gamer2->lose());
    if (viewMode == ViewMode::ALLPLAYER) {
      view->ShowState(gamer1, lastStrike1, score.first + score.second + 1);
      view->pause();
    }
    if (gamer2->lose()) {
      break;
    }
    lastStrike2 = Strike::UNKNOWN;
    do {
      if (viewMode == ViewMode::ALLPLAYER) {
        view->ShowState(gamer2, lastStrike2, score.first + score.second + 1);
        view->pause();
      }
    } while ((lastStrike2 = gamer2->move(gamer1)) != Strike::MISS && !gamer1->lose());
    if (viewMode == ViewMode::ALLPLAYER) {
      view->ShowState(gamer2, lastStrike2, score.first + score.second + 1);
      view->pause();
    }
  }
  if (gamer1->lose()) {
    ++score.second;
  } else {
    ++score.first;
  }
  view->showResultRound(std::pair{gamer1, gamer2});
  view->pause();
  gamer1->endRound();
  gamer2->endRound();
}
void Game::mainloop() {
  for (int i = 0; i < numberRounds; ++i) {
    playRound();
  }
  std::ofstream fout("result.txt");
  fout << "player 1: " << score.first << std::endl
       << "player 2: " << score.second << std::endl;
  fout.close();
}

Game::Game(int countRounds, GamerType typeGamer1, GamerType typeGamer2, int width,
           int height, ViewMode mode) {
  gamer1 = factory.create(typeGamer1, width, height, "Gamer 1");
  gamer2 = factory.create(typeGamer2, width, height, "Gamer 2");
  score.first = 0;
  score.second = 0;
  numberRounds = countRounds;
  viewMode = mode;
  view = new ConsoleView;
}
