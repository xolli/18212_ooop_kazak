#include <vector>

#include "Board.h"
#include "MyExceptions.h"

void Board::addShip(Ship added_ship) {
  if ((added_ship.get_direction() == Direction::RIGHT &&
       added_ship.get_coord().first + added_ship.get_length() > width) ||
      (added_ship.get_direction() == Direction::UP &&
       added_ship.get_coord().second + added_ship.get_length() > height) ||
      added_ship.get_coord().first >= width || added_ship.get_coord().second >= height) {
    throw OutOfTheBoard();
  }
  for (Ship s : ships) {
    if (s.overlap(added_ship)) {
      throw ShipsCross();
    }
  }
  ships.push_back(added_ship);
}

Strike Board::shot(std::pair<int, int> coord) {
  if (data[coord.first][coord.second]) {
    return Strike::REPLAY;
  }
  data[coord.first][coord.second] = true;
  for (Ship &s : ships) {
    if (s.hit(coord)) {
      return s.get_state() == State::DAMAGED ? Strike::HIT : Strike::KILL;
    }
  }
  return Strike::MISS;
}

bool Board::end() {
  for (Ship s : ships) {
    if (s.get_state() != State::KILLED) {
      return false;
    }
  }
  return true;
}
Board::Board(int setWidth, int setHeight) {
  if (setWidth < 0 || setHeight < 0) {
    throw BoardCreate();
  }
  width = setWidth;
  height = setHeight;
  data = std::vector<std::vector<bool>>(width, std::vector<bool>(height));
}

std::vector<std::vector<CellState>> Board::get_board() {
  auto result = std::vector<std::vector<CellState>>(
      width, std::vector<CellState>(height, CellState::WHOLEEMPTY));
  for (auto s : ships) {
    int x = s.get_coord().first;
    int y = s.get_coord().second;
    if (s.get_direction() == Direction::RIGHT) {
      for (int i = x; i <= x + s.get_length() - 1; ++i) {
        result[i][y] = (data[i][y]) ? CellState::SHOTSHIP : CellState::WHOLESHIP;
      }
    } else {
      for (int i = y; i <= y + s.get_length() - 1; ++i) {
        result[x][i] = (data[x][i]) ? CellState::SHOTSHIP : CellState::WHOLESHIP;
      }
    }
  }
  for (int i = 0; i < width; ++i) {
    for (int j = 0; j < height; ++j) {
      if (result[i][j] != CellState::SHOTSHIP && result[i][j] != CellState::WHOLESHIP &&
          data[i][j]) {
        result[i][j] = CellState::SHOTEMPTY;
      }
    }
  }
  return result;
}

int Board::get_height() { return height; }
int Board::get_width() { return width; }
