#include "../utility.h"
#include "Gamer.h"

#include <cstdlib>
#include <ctime>

RandomGamer::RandomGamer(int width, int height, std::string playerName) : Gamer(width, height, playerName) {
  srand(time(nullptr));
}

bool RandomGamer::correctCoord(std::vector<std::vector<bool>> const &cellBusy,
                               std::pair<int, int> coord, int length, Direction direct) {

  if (direct == Direction::RIGHT) {
    if (coord.first + length > widthBoard) {
      return false;
    }
    for (int x = coord.first; x <= coord.first + length - 1; ++x) {
      if (cellBusy[x][coord.second]) {
        return false;
      }
    }
  } else {
    if (coord.second + length > heightBoard) {
      return false;
    }
    for (int y = coord.second; y <= coord.second + length - 1; ++y) {
      if (cellBusy[coord.first][y]) {
        return false;
      }
    }
  }
  return true;
}

void RandomGamer::addShip(std::vector<std::vector<bool>> &cellBusy,
                          std::pair<int, int> coord, int length, Direction direct) {
  if (direct == Direction::RIGHT) {
    for (int x = maximum(0, coord.first - 1);
         x <= minimum(coord.first + length, widthBoard - 1); ++x) {
      for (int y = maximum(0, coord.second - 1);
           y <= minimum(coord.second + 1, heightBoard - 1); ++y) {
        cellBusy[x][y] = true;
      }
    }
  } else {
    for (int x = maximum(0, coord.first - 1);
         x <= minimum(coord.first + 1, widthBoard - 1); ++x) {
      for (int y = maximum(0, coord.second - 1);
           y <= minimum(coord.second + length, heightBoard - 1); ++y) {
        cellBusy[x][y] = true;
      }
    }
  }
  myBoard.addShip(Ship(coord, length, direct));
}

void RandomGamer::arrangeShips() {
  std::vector<std::vector<bool>> cellBusy(widthBoard,
                                          std::vector<bool>(heightBoard, false));
  for (int length = 4; length >= 1; --length) {
    for (int i = 0; i < 5 - length; ++i) {
      Direction direct = rand() % 2 ? Direction::RIGHT : Direction::UP;
      std::pair coord{abs(rand()) % widthBoard, abs(rand()) % heightBoard};
      while (!correctCoord(cellBusy, coord, length, direct)) {
        direct = rand() % 2 ? Direction::RIGHT : Direction::UP;
        coord = std::pair{abs(rand()) % widthBoard, abs(rand()) % heightBoard};
      }
      addShip(cellBusy, coord, length, direct);
    }
  }
}
Strike RandomGamer::move(class Gamer *enemy) {
  std::pair coord{abs(rand()) % widthBoard, abs(rand()) % heightBoard};
  while (enemyBoard[coord.first][coord.second] != EnemyCell::UNKNOWN) {
    coord = std::pair{abs(rand()) % widthBoard, abs(rand()) % heightBoard};
  }
  Strike r = enemy->take_hit(coord);
  if (r == Strike::HIT || r == Strike::KILL) {
    enemyBoard[coord.first][coord.second] = EnemyCell::SHIP;
  } else if (r == Strike::MISS) {
    enemyBoard[coord.first][coord.second] = EnemyCell::EMPTY;
  }
  return r;
}
