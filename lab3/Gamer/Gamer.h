#ifndef LAB3_GAMER_H
#define LAB3_GAMER_H

class ConsoleControl;
class ConsoleView;

enum class proveShip { GOOD, OUT_OF_THE_BOARD, MANY_SHIPS, INCORRECT_LENGTH };

#include "../GameView.h"
#include "../Board.h"
#include "../GameControl.h"

#include <set>
#include <string>

enum class GamerState { SEARCHSHIP4, SEARCHSHIP23, SEARCHSHIP1, BATTLE };

enum class GamerType { CONSOLE, RANDOM, OPTIMAL };

class Gamer {
protected:
  std::string name;
  Board myBoard;
  std::vector<std::vector<EnemyCell>> enemyBoard;
  int widthBoard, heightBoard;

public:
  explicit Gamer(int boardWidth, int boardHeight, std::string playerName);
  virtual void arrangeShips() = 0;
  virtual Strike move(Gamer *enemy) = 0;
  bool lose();
  Strike take_hit(std::pair<int, int>);
  virtual void endRound();
  std::string getName();
  friend class ConsoleView;
};

class HumanGamer : public Gamer {
  ConsoleControl *controller;
  ConsoleView *viewer;
  std::vector<int> countShip;
  std::pair<int, int> score;

  Strike lastStrike;
  proveShip validShip(Ship);

public:
  using Gamer::Gamer;
  HumanGamer(int widthBoard, int heightBoard, std::string playerName);
  ~HumanGamer();
  void arrangeShips() override;
  Strike move(Gamer *enemy) override;
  void endRound() override;
  friend class ConsoleControl;
};

class RandomGamer : public Gamer {
  bool correctCoord(std::vector<std::vector<bool>> const &cellBusy,
                    std::pair<int, int> coord, int length, Direction direct);
  void addShip(std::vector<std::vector<bool>> &cellBusy, std::pair<int, int> coord,
               int length, Direction direct);

public:
  using Gamer::Gamer;
  RandomGamer(int width, int height, std::string playerName);
  void arrangeShips() override;
  Strike move(Gamer *enemy) override;
};

class OptimalGamer : public Gamer {
  std::set<std::pair<int, int>> findShip23;
  std::set<std::pair<int, int>> findShip4;
  std::vector<int> countKillShip;
  GamerState curState;
  int lengthKillShip; // Длина последнего убитого корабля
  std::pair<int, int> curCoordBattle;
  void updateEnemyBoard(Strike resultStrike, std::pair<int, int> coord);
  void markArea(std::pair<int, int>);
  void updateState(Strike, std::pair<int, int>);
  std::pair<int, int> continueBattle();

  std::vector<std::vector<bool>> busy;
  void addLevel4Ships();
  void addLevel3Ships();
  void addLevel2Ships();
  void addLevel1Ships();
  int addBusyCells(Ship);
  int countAddedBusyCells(Ship);
  bool correctShip(Ship);

public:
  using Gamer::Gamer;
  OptimalGamer(int boardWidth, int boardHeight, std::string playerName);
  void arrangeShips() override;
  Strike move(Gamer *enemy) override;
  void endRound() override;
};

#endif // LAB3_GAMER_H
