#include "Gamer.h"
#include "../MyExceptions.h"

Gamer::Gamer(int boardWidth, int boardHeight, std::string playerName = "") {
  widthBoard = boardWidth;
  heightBoard = boardHeight;
  enemyBoard = std::vector<std::vector<EnemyCell>>(
      boardWidth, std::vector<EnemyCell>(boardHeight, EnemyCell::UNKNOWN));
  myBoard = Board(boardWidth, boardHeight);
  name = playerName;
}

Strike Gamer::take_hit(std::pair<int, int> coord_shot) {
  return myBoard.shot(coord_shot);
}

bool Gamer::lose() { return myBoard.end(); }

void Gamer::endRound() {
  enemyBoard = std::vector<std::vector<EnemyCell>>(
      WIDTH, std::vector<EnemyCell>(HEIGHT, EnemyCell::UNKNOWN));
  myBoard = Board(HEIGHT, WIDTH);
}
std::string Gamer::getName() { return name; }
