#include "../utility.h"
#include "Gamer.h"

#include <iostream>
#include <utility>

OptimalGamer::OptimalGamer(int boardWidth, int boardHeight, std::string playerName)
    : Gamer(boardWidth, boardHeight, playerName) {
  srand(time(NULL));
  busy =
      std::vector<std::vector<bool>>(boardWidth, std::vector<bool>(boardHeight, false));

  int beginX = 3;
  for (int y = 0; y < HEIGHT; ++y) {
    for (int x = beginX; x < WIDTH; x += 4) {
      findShip4.insert(std::pair{x, y});
    }
    beginX = (beginX == 0) ? 3 : beginX - 1;
  }

  beginX = 1;
  for (int y = 0; y < HEIGHT; ++y) {
    for (int x = beginX; x < WIDTH; x += 2) {
      findShip23.insert(std::pair{x, y});
    }
    beginX = (beginX == 0) ? 1 : beginX - 1;
  }
  curState = GamerState::SEARCHSHIP4;
  countKillShip = std::vector<int>(5, 0);
  lengthKillShip = 0;
  curCoordBattle.first = 0;
  curCoordBattle.second = 0;
}

static auto getRandElem(std::set<std::pair<int, int>> &s) {
  int index = abs(rand()) % s.size();
  auto it = s.begin();
  for (int i = 0; i != index; ++i) {
    ++it;
  }
  auto result = *it;
  s.erase(it);
  return result;
}

int OptimalGamer::countAddedBusyCells(Ship addedShip) {
  int result = 0;
  Direction direct = addedShip.get_direction();
  std::pair<int, int> coord = addedShip.get_coord();
  int length = addedShip.get_length();
  if (direct == Direction::RIGHT) {
    for (int x = maximum(0, coord.first - 1);
         x <= minimum(coord.first + length, widthBoard - 1); ++x) {
      for (int y = maximum(0, coord.second - 1);
           y <= minimum(coord.second + 1, heightBoard - 1); ++y) {
        result += !busy[x][y];
      }
    }
  } else {
    for (int x = maximum(0, coord.first - 1);
         x <= minimum(coord.first + 1, widthBoard - 1); ++x) {
      for (int y = maximum(0, coord.second - 1);
           y <= minimum(coord.second + length, heightBoard - 1); ++y) {
        result += !busy[x][y];
      }
    }
  }
  return result - length;
}

int OptimalGamer::addBusyCells(Ship addedShip) {
  Direction direct = addedShip.get_direction();
  std::pair<int, int> coord = addedShip.get_coord();
  int length = addedShip.get_length();
  if (direct == Direction::RIGHT) {
    for (int x = maximum(0, coord.first - 1);
         x <= minimum(coord.first + length, widthBoard - 1); ++x) {
      for (int y = maximum(0, coord.second - 1);
           y <= minimum(coord.second + 1, heightBoard - 1); ++y) {
        busy[x][y] = true;
      }
    }
  } else {
    for (int x = maximum(0, coord.first - 1);
         x <= minimum(coord.first + 1, widthBoard - 1); ++x) {
      for (int y = maximum(0, coord.second - 1);
           y <= minimum(coord.second + length, heightBoard - 1); ++y) {
        busy[x][y] = true;
      }
    }
  }
}

bool OptimalGamer::correctShip(Ship ship) {
  int x = ship.get_coord().first;
  int y = ship.get_coord().second;
  int length = ship.get_length();
  Direction direct = ship.get_direction();
  if (direct == Direction::RIGHT &&
      (x < 0 || x + length > WIDTH || y < 0 || y >= HEIGHT)) {
    return false; // выход за границы поля
  } else if (direct == Direction::UP &&
             (y < 0 || y + length > HEIGHT || x < 0 || x >= WIDTH)) {
    return false; // выход за границы поля
  }
  if (direct == Direction::RIGHT) {
    for (int i = x; i < x + length; ++i) {
      if (busy[i][y]) {
        return false;
      }
    }
  } else {
    for (int i = y; i < y + length; ++i) {
      if (busy[x][i]) {
        return false;
      }
    }
  }
  return true;
}

void OptimalGamer::addLevel4Ships() {
  Direction d = rand() % 2 ? Direction::UP : Direction::RIGHT;
  int x = 0, y = 0;
  if (d == Direction::UP) {
    x = rand() % 2 ? 0 : WIDTH - 1;
    y = abs(rand()) % (HEIGHT - 3);
  } else {
    x = abs(rand()) % (WIDTH - 3);
    y = rand() % 2 ? 0 : HEIGHT - 1;
  }
  Ship newShip(std::make_pair(x, y), 4, d);
  addBusyCells(newShip);
  myBoard.addShip(newShip);
}

void OptimalGamer::addLevel3Ships() {
  Direction d = rand() % 2 ? Direction::UP : Direction::RIGHT;
  int x = abs(rand()) % WIDTH, y = abs(rand()) % HEIGHT;
  Ship newShip(std::make_pair(x, y), 3, d);
  while (!correctShip(newShip) || countAddedBusyCells(newShip) >= 8) {
    d = rand() % 2 ? Direction::UP : Direction::RIGHT;
    x = abs(rand()) % WIDTH;
    y = abs(rand()) % HEIGHT;
    newShip = Ship(std::pair{x, y}, 3, d);
  }
  addBusyCells(newShip);
  myBoard.addShip(newShip);
}

void OptimalGamer::addLevel2Ships() {
  Direction d = rand() % 2 ? Direction::UP : Direction::RIGHT;
  int x = abs(rand()) % WIDTH, y = abs(rand()) % HEIGHT;
  Ship newShip(std::make_pair(x, y), 2, d);
  while (!correctShip(newShip) || countAddedBusyCells(newShip) >= 6) {
    d = rand() % 2 ? Direction::UP : Direction::RIGHT;
    x = abs(rand()) % WIDTH;
    y = abs(rand()) % HEIGHT;
    newShip = Ship(std::pair{x, y}, 2, d);
  }
  addBusyCells(newShip);
  myBoard.addShip(newShip);
}

void OptimalGamer::addLevel1Ships() {
  Direction d = Direction::RIGHT;
  int x = abs(rand()) % WIDTH, y = abs(rand()) % HEIGHT;
  Ship newShip(std::make_pair(x, y), 1, d);
  while (!correctShip(newShip)) {
    x = abs(rand()) % WIDTH;
    y = abs(rand()) % HEIGHT;
    newShip = Ship(std::pair{x, y}, 1, d);
  }
  addBusyCells(newShip);
  myBoard.addShip(newShip);
}

void OptimalGamer::arrangeShips() {
  addLevel4Ships();

  addLevel3Ships();
  addLevel3Ships();
  for (int i = 0; i < 3; ++i) {
    addLevel2Ships();
  }
  for (int i = 0; i < 4; ++i) {
    addLevel1Ships();
  }
}

Strike OptimalGamer::move(class Gamer *enemy) {
  Strike r;
  if (curState == GamerState::SEARCHSHIP4) {
    if (findShip4.empty()) {
      int x = 0;
    }
    auto coord = getRandElem(findShip4);
    findShip23.erase(coord);
    r = enemy->take_hit(coord);
    updateEnemyBoard(r, coord);
    updateState(r, coord);
  } else if (curState == GamerState::SEARCHSHIP23) {
    if (findShip23.empty()) {
      int x = 0;
    }
    auto coord = getRandElem(findShip23);
    findShip4.erase(coord);
    r = enemy->take_hit(coord);
    if (r == Strike::REPLAY) {
      int x = 0;
    }
    updateEnemyBoard(r, coord);
    updateState(r, coord);
  } else if (curState == GamerState::SEARCHSHIP1) {
    auto coord = std::pair{abs(rand()) % WIDTH, abs(rand()) % HEIGHT};
    while (enemyBoard[coord.first][coord.second] != EnemyCell::UNKNOWN) {
      coord = std::pair{abs(rand()) % WIDTH, abs(rand()) % HEIGHT};
    }
    findShip23.erase(coord);
    findShip4.erase(coord);
    r = enemy->take_hit(coord);
    updateEnemyBoard(r, coord);
    updateState(r, coord);
  } else {
    auto coord = continueBattle();
    findShip23.erase(coord);
    findShip4.erase(coord);
    r = enemy->take_hit(coord);
    updateEnemyBoard(r, coord);
    updateState(r, coord);
  }
  return r;
}
void OptimalGamer::updateEnemyBoard(Strike resultStrike, std::pair<int, int> coord) {
  enemyBoard[coord.first][coord.second] =
      (resultStrike == Strike::MISS) ? EnemyCell::EMPTY : EnemyCell ::SHIP;
  if (resultStrike != Strike::KILL) {
    return;
  }
  int x = coord.first;
  int y = coord.second;
  lengthKillShip = 1;
  while (x > 0 && enemyBoard[x - 1][y] == EnemyCell::SHIP) {
    --x; // идём в крайнюю левую точку
  }
  while (y > 0 && enemyBoard[x][y - 1] == EnemyCell::SHIP) {
    --y; // идём в крайнюю нижнюю
  }
  // на этом моменте мы знаем, что слева и снизу продолжения корабля быть не может

  std::pair right{x + 1, y};
  std::pair up{x, y + 1};
  markArea(std::pair{x, y});
  if (x < WIDTH - 1 && enemyBoard[x + 1][y] == EnemyCell::SHIP) {
    while (x < WIDTH - 1 && enemyBoard[x + 1][y] == EnemyCell::SHIP) {
      ++x;
      ++lengthKillShip;
      markArea(std::pair{x, y});
    }
  } else if (y < HEIGHT - 1 && enemyBoard[x][y + 1] == EnemyCell::SHIP) {
    while (y < HEIGHT - 1 && enemyBoard[x][y + 1] == EnemyCell::SHIP) {
      ++y;
      ++lengthKillShip;
      markArea(std::pair{x, y});
    }
  }
}
void OptimalGamer::markArea(std::pair<int, int> coord) {
  for (int x = maximum(0, coord.first - 1); x <= minimum(coord.first + 1, WIDTH - 1);
       ++x) {
    for (int y = maximum(0, coord.second - 1); y <= minimum(coord.second + 1, HEIGHT - 1);
         ++y) {
      if (enemyBoard[x][y] != EnemyCell::SHIP) {
        enemyBoard[x][y] = EnemyCell::EMPTY;
        findShip23.erase(std::pair{x, y});
        findShip4.erase(std::pair{x, y});
      }
    }
  }
}
void OptimalGamer::updateState(Strike resultStrike, std::pair<int, int> coordStrike) {
  if (resultStrike == Strike::HIT) {
    curCoordBattle = coordStrike;
    curState = GamerState::BATTLE;
  } else if (resultStrike == Strike::KILL && curState == GamerState::BATTLE) {
    ++countKillShip[lengthKillShip];
    if (countKillShip[4] == 0) {
      curState = GamerState::SEARCHSHIP4;
    } else if (countKillShip[3] != 2 || countKillShip[2] != 3) {
      if (findShip23.empty()) {
        int x = 0;
      }
      curState = GamerState::SEARCHSHIP23;
    } else {
      curState = GamerState::SEARCHSHIP1;
    }
  } else if (!(curState == GamerState::BATTLE && resultStrike == Strike::MISS)) {
    countKillShip[lengthKillShip] += (resultStrike == Strike::KILL);
    if (countKillShip[4] == 0) {
      curState = GamerState::SEARCHSHIP4;
    } else if (countKillShip[3] != 2 || countKillShip[2] != 3) {
      if (findShip23.empty()) {
        int x = 0;
      }
      curState = GamerState::SEARCHSHIP23;
    } else {
      curState = GamerState::SEARCHSHIP1;
    }
  }
}
std::pair<int, int> OptimalGamer::continueBattle() {
  int x_begin = curCoordBattle.first;
  int y_begin = curCoordBattle.second;
  int x = x_begin;
  int y = y_begin;

  while (x > 0 && enemyBoard[x - 1][y] == EnemyCell::SHIP) {
    --x; // идём в крайнюю левую точку
  }
  while (y > 0 && enemyBoard[x][y - 1] == EnemyCell::SHIP) {
    --y; // идём в крайнюю нижнюю
  }
  // на этом моменте мы знаем, что слева и снизу известного продолжения корабля быть не
  // может

  if (x < WIDTH - 1 && enemyBoard[x + 1][y] == EnemyCell::SHIP && x > 0 &&
      enemyBoard[x - 1][y] == EnemyCell::UNKNOWN) {
    return std::pair{x - 1, y};
  }
  if (y < HEIGHT - 1 && enemyBoard[x][y + 1] == EnemyCell::SHIP && y > 0 &&
      enemyBoard[x][y - 1] == EnemyCell::UNKNOWN) {
    return std::pair{x, y - 1};
  }

  while (x < WIDTH - 1 && enemyBoard[x + 1][y] == EnemyCell::SHIP) {
    ++x; // идём в крайнюю правую точку
  }
  while (y < HEIGHT && enemyBoard[x][y + 1] == EnemyCell::SHIP) {
    ++y; // идём в крайнюю верхнюю точку
  }
  // на этом моменте мы знаем, что справа и сверху известного продолжения корабля быть не
  // может

  if (x > 0 && enemyBoard[x - 1][y] == EnemyCell::SHIP && x < WIDTH - 1 &&
      enemyBoard[x + 1][y] == EnemyCell::UNKNOWN) {
    return std::pair{x + 1, y};
  }
  if (y > 0 && enemyBoard[x][y - 1] == EnemyCell::SHIP && y < HEIGHT - 1 &&
      enemyBoard[x][y + 1] == EnemyCell::UNKNOWN) {
    return std::pair{x, y + 1};
  }

  if (x > 0 && enemyBoard[x - 1][y] == EnemyCell::UNKNOWN) {
    return std::pair{x - 1, y};
  }
  if (x < WIDTH - 1 && enemyBoard[x + 1][y] == EnemyCell::UNKNOWN) {
    return std::pair{x + 1, y};
  }
  if (y < HEIGHT - 1 && enemyBoard[x][y + 1] == EnemyCell::UNKNOWN) {
    return std::pair{x, y + 1};
  }
  if (y > 0 && enemyBoard[x][y - 1] == EnemyCell::UNKNOWN) {
    return std::pair{x, y - 1};
  }
}
void OptimalGamer::endRound() {
  Gamer::endRound();
  busy = std::vector<std::vector<bool>>(WIDTH, std::vector<bool>(HEIGHT, false));

  findShip4.clear();
  int beginX = 3;
  for (int y = 0; y < HEIGHT; ++y) {
    for (int x = beginX; x < WIDTH; x += 4) {
      findShip4.insert(std::pair{x, y});
    }
    beginX = (beginX == 0) ? 3 : beginX - 1;
  }

  findShip23.clear();
  beginX = 1;
  for (int y = 0; y < HEIGHT; ++y) {
    for (int x = beginX; x < WIDTH; x += 2) {
      findShip23.insert(std::pair{x, y});
    }
    beginX = (beginX == 0) ? 1 : beginX - 1;
  }

  curState = GamerState::SEARCHSHIP4;
  countKillShip = std::vector<int>(5, 0);
  lengthKillShip = 0;
  curCoordBattle.first = 0;
  curCoordBattle.second = 0;
}
