#include "GamerFactory.h"

GamerFactory::GamerFactory() {
  factoryMap[GamerType::CONSOLE] = new GamerCreator<HumanGamer>;
  factoryMap[GamerType::RANDOM] = new GamerCreator<RandomGamer>;
  factoryMap[GamerType::OPTIMAL] = new GamerCreator<OptimalGamer>;
}

GamerFactory::~GamerFactory() {
  for (auto elem : factoryMap) {
    delete elem.second;
    elem.second = nullptr;
  }
}
Gamer *GamerFactory::create(GamerType type, int width, int height, std::string name) const {
  auto it = factoryMap.find(type);
  if (it != factoryMap.end()) {
    return it->second->create(width, height, name);
  } else {
    return nullptr;
  }
}
