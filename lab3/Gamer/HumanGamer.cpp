#include <iostream>
#include <sstream>
#include <tuple>

#include "../GameView.h"
#include "../MyExceptions.h"
#include "Gamer.h"

HumanGamer::HumanGamer(int widthBoard, int heightBoard, std::string playerName) : Gamer(widthBoard, heightBoard, playerName) {
  countShip = std::vector<int>(4, 0);
  lastStrike = Strike::UNKNOWN;
  score = std::pair{0, 0};
  viewer = new ConsoleView;
  controller = new ConsoleControl;
  name = controller->getPlayerName();
}
HumanGamer::~HumanGamer() {
  delete viewer;
  delete controller;
}

void HumanGamer::arrangeShips() {
  viewer->ShowState(this, lastStrike, score.first + score.second + 1);
  for (int length = 1; length <= 4; ++length) {
    for (int i = 0; i < 5 - length; ++i) {
      try {
        Ship newShip = controller->getShip();
        proveShip resultProve = validShip(newShip);
        while (resultProve != proveShip::GOOD) {
          if (resultProve == proveShip::MANY_SHIPS) {
            viewer->tryAgain("Many replays ship's length");
          } else if (resultProve == proveShip::INCORRECT_LENGTH) {
            viewer->tryAgain("Incorrect length");
          } else if (resultProve == proveShip::OUT_OF_THE_BOARD) {
            viewer->tryAgain("Out of the board");
          }
          newShip = controller->getShip();
          resultProve = validShip(newShip);
        }
        myBoard.addShip(newShip);
        ++countShip[newShip.get_length()];
        viewer->ShowState(this, lastStrike, score.first + score.second + 1);
      } catch (std::exception &e) {
        viewer->tryAgain(e.what());
        --i;
      }
    }
  }
}

Strike HumanGamer::move(class Gamer *enemy) {
  viewer->ShowState(this, lastStrike, score.first + score.second + 1);
  std::pair<int, int> coord = controller->getCoordStrike(*this);
  lastStrike = enemy->take_hit(coord);
  viewer->ShowState(this, lastStrike, score.first + score.second + 1);
  if (lastStrike == Strike::KILL) {
    enemyBoard[coord.first][coord.second] = EnemyCell::SHIP;
  } else if (lastStrike == Strike::HIT) {
    enemyBoard[coord.first][coord.second] = EnemyCell::SHIP;
  } else if (lastStrike == Strike::MISS) {
    enemyBoard[coord.first][coord.second] = EnemyCell::EMPTY;
  }
  return lastStrike;
}

proveShip HumanGamer::validShip(Ship ship) {
  if (ship.get_coord().first > WIDTH || ship.get_coord().first < 0 ||
      ship.get_coord().second > HEIGHT || ship.get_coord().second < 0) {
    return proveShip::OUT_OF_THE_BOARD;
  } else if (ship.get_length() <= 0 || ship.get_length() > 4) {
    return proveShip::INCORRECT_LENGTH;
  } else if (countShip[ship.get_length()] + 1 > 5 - ship.get_length()) {
    return proveShip ::MANY_SHIPS;
  }
  return proveShip ::GOOD;
}
void HumanGamer::endRound() {
  if (!lose()) {
    ++score.first;
  } else {
    ++score.second;
  }
  Gamer::endRound();
  lastStrike = Strike::UNKNOWN;
  countShip = std::vector<int>(4, 0);
}
