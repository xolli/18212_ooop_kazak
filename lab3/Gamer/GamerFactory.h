#ifndef LAB3_GAMERFACTORY_H
#define LAB3_GAMERFACTORY_H

#include "Gamer.h"
#include <map>

class AbstractGamerCreator {
public:
  virtual Gamer *create(int width, int height, std::string name) = 0;
};

template <class G> class GamerCreator : public AbstractGamerCreator {
public:
  Gamer *create(int width, int height, std::string name) override { return new G(width, height, name); };

  ~GamerCreator() = default;
};

class GamerFactory {
  std::map<GamerType, AbstractGamerCreator *> factoryMap;

public:
  GamerFactory();

  ~GamerFactory();

  Gamer *create(GamerType, int width, int height, std::string name) const;
};

#endif // LAB3_GAMERFACTORY_H
